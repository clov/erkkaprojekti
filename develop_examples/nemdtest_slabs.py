from nemd import NEMD
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices
from ase import units
from lammpsLib import LAMMPSLib
from ase.calculators.lammps import LAMMPS
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution,\
     Stationary, ZeroRotation
from ase.constraints import FixAtoms
import pickle, time
from helpfunctions import get_zgnr_width_angstroms, center_atoms,\
     setup_bending
from md_utilities import ExternalForceLJ, save_heat_power, save_energies
from md_utilities import save_temp_profile, print_step, save_temp_function

start = time.clock()

width = 5 
length = 130
atoms = GNR(width, length, 'zigzag')
width_Ang = get_zgnr_width_angstroms(width) 
#TODO change this
length_Ang = 318.507
print 'Atoms in the simulation: {0}'.format(len(atoms))
atoms.set_pbc(False)
atoms.center(vacuum=1.0)

alpha = 0.5
temp = 300
temp_cold = (1-alpha) * temp
temp_hot = (1+alpha) * temp

x, y, z = atoms.get_cell().diagonal()
Nslabs = length / 2
regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
slabs = AllSlabs(atoms, regions)
Nbaths = length / 40
#slabs.set_cold_slabs(range(Nslabs-Nbaths, Nslabs))
slabs.set_cold_slabs(range(0, Nbaths))
slabs.set_cold_target_temperature(temp_cold)
#slabs.set_hot_slabs(range(0, Nbaths))
slabs.set_hot_slabs(range(Nslabs-Nbaths, Nslabs))
slabs.set_hot_target_temperature(temp_hot)

"""
theta = 0.05
R = width_Ang / (2*theta)
alpha = 100. / R
atoms = setup_bending(atoms, alpha, R)
"""

atoms.center(vacuum=50.)

# Fix the ends
fix_ends_indices = get_zgnr_end_indices(width, length)
fix_ends_constraint = FixAtoms(indices=fix_ends_indices)

if True:
    # Simulate a surface
    rm = 3.0
    surface_pos = atoms.positions[0][1] - rm
    surface_constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0),
                         surface_pos, eps=20e-3, rm=rm, cutoff=10.0)

    atoms.set_constraint([surface_constraint, fix_ends_constraint])

else:
    atoms.set_constraint(fix_ends_constraint)

MaxwellBoltzmannDistribution(atoms, temp*units.kB)

dyn = NEMD(atoms, 0.2*units.fs, 600*units.fs, slabs)
steps = int(1e3)
steps2 = int(1e2)
stepstot = steps + steps2

f_temp = open('data/nemdtest_slabs_temp_profile.dat', 'w')
save_temp_profile(f_temp, dyn)
dyn.attach(save_temp_profile, stepstot/40, f_temp, dyn)
f_temp_function = 'data/temps/nemdtest_slabs_temp_function'
save_temp_function(dyn, f_temp_function)
dyn.attach(save_temp_function, steps/10, dyn, f_temp_function)
f_heat_power = open('data/nemdtest_slabs_heat_power.dat', 'w')
dyn.attach(save_heat_power, steps/100, f_heat_power, dyn)
f_energies = open('data/nemdtest_slabs_energies.dat', 'w')
dyn.attach(save_energies, steps/200, f_energies, dyn, atoms)
dyn.attach(print_step, 1, dyn)

#cell_cp = 0.5 * np.array(atoms.get_cell().diagonal())
#dyn.attach(center_atoms, int(1e4), atoms, cell_cp)

traj = PickleTrajectory('traj/nemdtest_slabs.traj', 'w', atoms)
if stepstot < 500:
    interval = 1
else:
    interval = stepstot / 500
dyn.attach(traj.write, interval)

parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
#parameters = ['pair_style tersoff', 'pair_coeff * * SiCGe.tersoff C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

"""
parameters = {'pair_style':'airebo 3.0',
              'pair_coeff':['* * CH.airebo C'],
              'mass':['* 12.0107'],
              'boundary':'f f f'}
calc = LAMMPS(tmp_dir='./lammps', parameters=parameters,
              files=['CH.airebo'])
atoms.set_calculator(calc)
"""

dyn.run(steps)

dyn.attach(save_temp_function, steps2/10, dyn, f_temp_function)
area = width_Ang * 1.42
coeff, coeff_err = dyn.run_and_calc(steps2)
coeff = coeff / area * length_Ang
coeff_err = coeff_err / area * length_Ang
joule = units.kJ / 1000
meter = 1e10
factor = 1. / (joule / (units.second*meter) )
coeff_si = factor * coeff 
coeff_err_si = factor * coeff_err
print 'Heat transfer coefficient: ({0}+-{1}) W/mK'.format(coeff_si,
                                                       coeff_err_si)

f_temp.close()
f_heat_power.close()
f_energies.close()

print 'Elapsed time: {0} s'.format(time.clock() - start)

