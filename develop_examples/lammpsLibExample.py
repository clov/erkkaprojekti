"""Example Molecular Dynamics using LAMMPS with LJ pairstyle"""

from ase import units
from ase.lattice.cubic import BodyCenteredCubic
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.md.verlet import VelocityVerlet
from ase.visualize import view

from lammpsLib import LAMMPSLib

#Setup Lattice
lattice_constant = 3.36
size = 4
cube = BodyCenteredCubic(symbol='Cu', latticeconstant=lattice_constant, size=(size,size,size))

#Setup LAMMPS Library version calculator
cmds_to_pass = ["pair_style lj/cut 3", "pair_coeff * * 1 3"]
log = "lammps.log"
type_dict = {"Cu":1} #With one element, this is not neccesary. It's here for example.
calc = LAMMPSLib(parameters=cmds_to_pass, atom_types=type_dict, log_file=log)
cube.set_calculator(calc) 

#Run Dyanmics
MaxwellBoltzmannDistribution(cube, 300*units.kB)
dyn = VelocityVerlet(cube, 2*units.fs) 
traj = PickleTrajectory("lammpsEG.traj", 'w', cube)
dyn.attach(traj.write, interval=2)
dyn.run(100)
#view(cube)
