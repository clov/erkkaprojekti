import os, sys
from md_utilities import plot_slope_evolution

home = os.getenv('HOME')
datapath = home+'/harjoittelu_2012/erkkaprojekti/examples/data/temps/'
outputpath = home+'/harjoittelu_2012/erkkaprojekti/examples/figs/'

fname_start = sys.argv[1]
#fname_start = 'mptest_slabs_nonperiodic'
#fname_start = 'nemdtest_slabs'

plot_slope_evolution(datapath, fname_start, 'dat', outputpath, 'png')
