from graphene_structures import GNR
from ase.lattice.cubic import Diamond
from ase.md.npt import NPT
from ase import units
from ase.io.trajectory import PickleTrajectory
from md_utilities import plot_temp_function, print_step
from lammpsLib import LAMMPSLib
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution

atoms = GNR(5, 40, 'zigzag')
#atoms = Diamond(symbol='C', directions=[(1,0,0), (0,1,0), (0,0,1)],\
#                size=(5,5,5))
atoms.set_pbc(False)
atoms.center(vacuum=100.0)

temp = 10 * units.kB
MaxwellBoltzmannDistribution(atoms, temp)

parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

steps = 100

dt = 0.2 * units.fs
dyn = NPT(atoms, dt, temp, 0.0, 10*dt, None)
traj = PickleTrajectory('traj/npttest.traj', 'w', atoms)
dyn.attach(traj.write, 1)
#dyn.attach(print_step, 100, dyn)
dyn.run(steps)
