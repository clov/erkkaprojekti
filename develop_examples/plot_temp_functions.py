import os
from md_utilities import plot_temp_functions

home = os.getenv('HOME')
path = home+'/harjoittelu_2012/erkkaprojekti/examples/{0}/temps/'
datapath = path.format('data')
figpath = path.format('figs')

plot_temp_functions(datapath, 'dat', figpath, 'png')
