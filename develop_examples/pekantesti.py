from ase import *
from ase.calculators.emt import EMT
from ase.md import VelocityVerlet
from pylab import *
from ase.md.npt import NPT
from ase.io import PickleTrajectory 
from ase import io


atoms = io.read('aucluster.xyz') #Atoms('Au2',[(0,0,0),(3,0,0)])
calc = EMT()
atoms.set_calculator(calc)

#md = VelocityVerlet(atoms,dt=units.fs)
md = NPT(atoms,timestep=5*units.fs,temperature=100*units.kB,externalstress=0,ttime=units.fs*50,pfactor=None)
traj = PickleTrajectory('traj/pekantesti.traj','w',atoms)
md.attach(traj.write)

print atoms.get_pbc()

md.run(steps=1000)

etot,epot,ekin=[],[],[]
for i in range(20000):
 md.run(steps=1)
 ep = atoms.get_potential_energy()
 ek = atoms.get_kinetic_energy()
 etot.append(ep+ek)
 epot.append(ep)
 ekin.append(ek)
 print i


print 'Temp from Ekin (K):',2*array(ekin).mean()/(3*(len(atoms)-2./3)*units.kB)

plot(etot)
plot(epot)
show()
