from scipy.stats import linregress
import matplotlib.pyplot as plt, numpy as np

x = range(5)
y = [0., 1., 2.5, 3., 4.]
plt.plot(x, y, 'ko')

slope, intercept, _, _, stderr = linregress(x, y)
print slope
print intercept
print stderr
xs = np.linspace(0, 5, 200)
poly = np.poly1d([slope, intercept])
plt.plot(xs, poly(xs), 'r-')
plt.show()
