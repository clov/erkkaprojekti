from mullerplathe import MullerPlathe
from regions import BoxRegions
from graphene_structures import GNR 
from ase import units
from ase.calculators.lammps import LAMMPS
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
import pickle, time

def save_temp_profile(f_temp, regions, atoms):
    pickle.dump(regions.get_temperatures(atoms), f_temp)

start = time.clock()

atoms = GNR(8, 60, 'zigzag')
print 'Atoms in the simulation: ' + str(len(atoms))
atoms.set_pbc(False)
atoms.center(vacuum=8)
MaxwellBoltzmannDistribution(atoms, 300*units.kB)

cell = atoms.get_cell()
x, y, z = cell[0][0], cell[1][1], cell[2][2]
regions = BoxRegions((0, x), (0, y), (0, z), 30, 'z')
cold_idxs = [0, 1, 2, 3, 4]
hot_idxs = [25, 26, 27, 28, 29]
dyn = MullerPlathe(atoms, 0.5*units.fs, 0, regions, cold_idxs, hot_idxs, 'C')

steps = 50000

f_temp = open('data/mptest_temp_profile.dat', 'w')
dyn.attach(save_temp_profile, steps/40, f_temp, regions, atoms)

traj = PickleTrajectory('traj/mptest.traj', 'w', atoms)
if steps < 500:
    traj_steps = steps
else:
    traj_steps = steps / 500
dyn.attach(traj.write, traj_steps)

"""
parameters = {'pair_style' : 'airebo 3.0',
              'pair_coeff' : ['* * CH.airebo C'],
              'mass' : ['* 12.0107'],
              'boundary' : 'f_temp f_temp f_temp'}
calc = LAMMPS(files=['CH.airebo'], parameters=parameters)
"""
parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

dyn.run(steps)

f_temp.close()

print 'Elapsed time: ' + str(time.clock() - start) + ' s'
