import os
from md_utilities import plot_temp_profiles

home = os.getenv('HOME')
path = home+'/harjoittelu_2012/erkkaprojekti/examples/{0}/'
datapath = path.format('data')
figpath = path.format('figs')

plot_temp_profiles(datapath, 'dat', figpath, 'png')
