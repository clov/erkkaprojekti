import numpy
from mayavi import mlab

s = numpy.abs(numpy.random.random((3, 3)))
mlab.barchart(s)
mlab.view(90, 180, 260)
mlab.savefig('mayavitest.png')
