import pickle, numpy as np
import matplotlib.pyplot as plt
from enthought.mayavi import mlab

def plot_temps():
    f_temp = open('data/mptest_slabs_temp_profile.dat', 'r')
    
    all_temps = []
    try:
        while True:
            step, temps = pickle.load(f_temp)
            #print step, temps
            all_temps.append(temps)
    except EOFError:
        pass
    
    f_temp.close()
    mlab.barchart(all_temps, scale_factor=0.05)
    mlab.show()

def plot_energies():
    f_energies = open('data/mptest_slabs_energies.dat', 'r')
    
    steps = []
    epots = []
    ekins = []
    etots = []
    try:
        while True:
            step, epot, ekin = pickle.load(f_energies)
            etot = epot + ekin
            steps.append(step)
            epots.append(epot)
            ekins.append(ekin)
            etots.append(etot)
    except EOFError:
        pass
    
    f_energies.close()
    plt.subplot(1, 3, 1)
    plt.plot(steps, np.array(epots), 'k-')
    plt.title('potential')
    plt.subplot(1, 3, 2)
    plt.plot(steps, np.array(ekins), 'b-')
    plt.title('kinetic')
    plt.subplot(1, 3, 3)
    plt.plot(steps, np.array(etots), 'g-')
    plt.title('total')
    plt.show()

if __name__ == '__main__':
    plot_temps()
    #plot_energies()