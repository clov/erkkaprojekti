from ase import Atoms
from regions import BoxRegions
from slabs import AllSlabs
from filter import EnhancedFilter

pos = [(0, 0, 0), (0, 1, 0),
       (0, 0, 1), (0, 1, 1),
       (0, 0, 2), (0, 1, 2),
       (0, 0, 3), (0, 1, 3)]
atoms = Atoms('C8', positions=pos)
regions = BoxRegions((-1, 1), (-1, 2), (-0.5, 3.5), 4, 'z')
slabs = AllSlabs(atoms, regions)

slabs.set_cold_slabs([0])
slabs.set_hot_slabs([2, 3])

print slabs.get_cold_atoms().index
