import os, sys
from md_utilities import plot_temp_profile

home = os.getenv('HOME')
path = home+'/harjoittelu_2012/erkkaprojekti/examples/{0}/'
datapath = path.format('data')
figpath = path.format('figs')

fname = sys.argv[1]

plot_temp_profile(datapath, fname, 'dat', figpath, 'png')
