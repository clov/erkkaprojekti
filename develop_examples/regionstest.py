import numpy as np
from helpfunctions import *
from regions import WedgeRegion, WedgeRegions
from ase import Atoms

transform = transform_cylindrical_to_cartesian
pos = [transform((1.5, -np.pi/3, 0)), transform((1.5, -np.pi/6, 0)),
       transform((1.5, np.pi/6, 0)), transform((1.5, np.pi/3, 0))]
vel = [(-1, 0, 1), (2, 0, 0), (-1, -1, 1), (0, 0, 1)]
atoms = Atoms('CHOSi', positions=pos)
atoms.set_velocities(vel)

regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=1)
assert are_equal(regions.get_temperature(atoms),
                 100704.80742743785)

regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=2)
assert are_equal(regions.get_temperature(atoms),
                 100704.80742743785)
assert are_equal(regions.regions[0].get_temperature(atoms),
                 27129.032131334476)
assert are_equal(regions.regions[1].get_temperature(atoms),
                 73575.775296103369)
