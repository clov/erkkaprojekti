from mullerplathe import MullerPlathe
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices 
from ase import units
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution,\
     Stationary, ZeroRotation
from ase.visualize import view
from ase.constraints import FixAtoms
import pickle, time, sys, numpy as np
from helpfunctions import get_zgnr_width_angstroms, center_atoms,\
     setup_bending
from md_utilities import ExternalForceLJ, print_step, save_temp_profile,\
     save_temp_function

fname = sys.argv[0].split('.')[0]

start = time.clock()

width = 5
length = 130
atoms = GNR(width, length, 'zigzag')
width_Ang = get_zgnr_width_angstroms(width) 
print 'Atoms in the simulation: {}'.format(len(atoms))
atoms.set_pbc(False)
atoms.center(vacuum=1.)

x, y, z = atoms.get_cell().diagonal()
Nslabs = 80
regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
slabs = AllSlabs(atoms, regions)
slabs.set_cold_slabs(range(0, 2))
slabs.set_hot_slabs(range(Nslabs-2, Nslabs))

atoms.center(vacuum=50.)
fix_ends_indices = get_zgnr_end_indices(width, length)
fix_ends_constraint = FixAtoms(indices=fix_ends_indices)
atoms.set_constraint(fix_ends_constraint)

"""
rm = 3.0
surface_pos = atoms.positions[0][1] - rm
constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0), surface_pos,
                             eps=20e-3, rm=rm, cutoff=10.0)
atoms.set_constraint(constraint)
"""
#np.random.seed(1)
MaxwellBoltzmannDistribution(atoms, 30*units.kB)

dyn = MullerPlathe(atoms, 0.2*units.fs, 80, slabs, 'C')

steps = int(1e2)
steps2 = int(1e2)
stepstot = steps + steps2

f_temp = open('data/{0}_temp_profile.dat'.format(fname), 'w')
#f_energies = open('data/mptest_slabs_energies.dat', 'w')
save_temp_profile(f_temp, dyn)
dyn.attach(save_temp_profile, stepstot/40, f_temp, dyn)
f_temp_function = 'data/temps/{0}_temp_function'.format(fname)
save_temp_function(dyn, f_temp_function)
dyn.attach(save_temp_function, steps/10, dyn, f_temp_function)
#dyn.attach(save_energies, steps/200, f_energies, dyn, atoms)

#dyn.attach(stationary_and_zerorotation, 10, atoms)
"""
cell_cp = 0.5 * np.array(atoms.get_cell().diagonal())
dyn.attach(center_atoms, int(2e4), atoms, cell_cp)
"""
dyn.attach(print_step, int(1e4), dyn)

traj = PickleTrajectory('traj/{0}.traj'.format(fname), 'w', atoms)
if stepstot < 500:
    interval = 1
else:
    interval = stepstot / 500
dyn.attach(traj.write, interval)

parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
#parameters = ['pair_style tersoff', 'pair_coeff * * SiCGe.tersoff C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

dyn.run(steps)
print 'Equilibration done, starting to calculate the heat transfer \
coefficient'

dyn.attach(save_temp_function, steps2/10, dyn, f_temp_function)
area = width_Ang * 1.42
coeff = dyn.run_and_calc(steps2) / area
joule = units.kJ / 1000
meter = 1e10
coeff_si = coeff / (joule / (units.second*meter) )
print 'Heat transfer coefficient: {} (eV/aut)/AK = {} W/mK'.format(coeff,
                                                            coeff_si)

f_temp.close()
#f_energies.close()

print 'Elapsed time: {} s'.format(time.clock() - start)
