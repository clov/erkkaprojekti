import pickle, sys
import matplotlib.pyplot as plt

fname = 'run_mullerplathe_bending'

thetas, coeffs, coeff_errs = [], [], []
with open('data/{0}_coeffs.dat'.format(fname), 'r') as f:
    while True:
        try:
            theta, coeff, coeff_err = pickle.load(f)
            thetas.append(theta)
            coeffs.append(coeff)
            coeff_errs.append(coeff_err)
        except(EOFError):
            break

plt.errorbar(thetas, coeffs, coeff_errs, fmt='k.')
plt.xlabel('$\Theta$')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')
plt.savefig('figs/{0}_coeffs.pdf'.format(fname))
plt.show()

