from multiprocessing import Process, Queue
import time, pickle

def run(theta, queue):
    time.sleep(1)
    queue.put(theta + 1)

if __name__ == '__main__':
    start = time.time()
    queue = Queue()
    processes = [Process(target=run, args=(theta, queue))\
                 for theta in range(0, 5)]
    for p in processes:
        p.start()
    with open('multiprocessingtest.dat', 'w') as f:
        for p in processes:
            pickle.dump(queue.get(), f)
            p.join()

    print 'Time elapsed: {0} s'.format(time.time() - start)
