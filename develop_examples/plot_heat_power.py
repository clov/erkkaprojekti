from md_utilities import plot_heat_power
import sys

datapath = 'data/'
figpath = 'figs/'
filename = sys.argv[1]

plot_heat_power(datapath, filename, 'dat', figpath, 'png')
