import pickle, sys
import matplotlib.pyplot as plt

fname = sys.argv[1]

ttimes, coeffs = [], []
with open(fname, 'r') as f:
    while True:
        try:
            ttime, coeff = pickle.load(f)
            ttimes.append(ttime)
            coeffs.append(coeff)
        except(EOFError):
            break

plt.plot(ttimes, coeffs, 'ko')
#plt.ylim([-1e3, 1e3])
plt.xlabel('ttime')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')
plt.show()

