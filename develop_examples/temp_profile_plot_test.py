import numpy as np
import matplotlib.pyplot as plt

steps = [0, 1000, 2000, 3000]
temps0 = [300, 300, 300, 300, 300]
temps1 = [280, 290, 300, 310, 320]
temps2 = [260, 270, 290, 310, 330]
temps3 = [250, 280, 310, 340, 370]
temps = [temps0, temps1, temps2, temps3]


xs = [[steps[i]] * len(temps[0]) for i in range(len(steps))]
ys = [range(len(temps[0])) for i in range(len(steps))]

plt.scatter(xs, ys, s=3000, c=temps, marker='s')

"""
plt.scatter([steps[0]]*5, ys, c=temps0)
plt.scatter([steps[1]]*5, ys, c=temps1)
plt.scatter([steps[2]]*5, ys, c=temps2)
plt.scatter([steps[3]]*5, ys, c=temps3)
"""

plt.show()

