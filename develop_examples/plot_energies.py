import os, sys
from md_utilities import plot_energies

home = os.getenv('HOME')
path = home+'/harjoittelu_2012/erkkaprojekti/examples/{0}/'
datapath = path.format('data')
figpath = path.format('figs')

fname = sys.argv[1]

plot_energies(datapath, fname, 'dat', figpath, 'png')
