import numpy as np
import matplotlib.pyplot as plt

steps = range(100)
dx = steps[-1] / 4
x = range(0, steps[-1]+1, dx)
y = [0, 1, 2, 3, 4, 5]
temps0 = [300, 300, 300, 300, 300]
temps1 = [290, 300, 300, 300, 310]
temps2 = [280, 290, 300, 310, 230]
temps3 = [270, 280, 290, 310, 330]

C = np.array([temps0, temps1, temps2, temps3])

X, Y = np.meshgrid(x, y)
plt.pcolormesh(X, Y, C.T)
plt.colorbar()
plt.show()
