from md_utilities import plot_heat_power_evolution
import sys

datafilename = sys.argv[1]

plot_heat_power_evolution(datafilename, outputdir='figs')
