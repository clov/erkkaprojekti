from md_utilities import plot_energy_evolution
import sys

datafilename = sys.argv[1]

plot_energy_evolution(datafilename, outputdir='figs')
