# Tested with Python 2.7.5, ASE 3.8.1, lammps-16Aug12 and the very
# earliest version of the LAMMPSLib library; the newer LAMMPSlib
# library needs a newer version of LAMMPS.

from nemd import NEMD
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices
from ase import units
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.constraints import FixAtoms
import sys, numpy as np
from helpfunctions import get_zgnr_width_angstroms,\
        get_zgnr_length_angstroms
from md_utilities import ExternalForceLJ, save_heat_power,\
     save_temp_profile, save_temp_distribution, save_energies,\
     save_temp_gradient, print_step

fname = sys.argv[0].split('.')[0]

width = 5 
length = 100
atoms = GNR(width, length, 'zigzag')
width_Ang = get_zgnr_width_angstroms(width) 
length_Ang = get_zgnr_length_angstroms(length)
atoms.set_pbc(False)
atoms.center(vacuum=1.)

print 'Atoms in the simulation: {0}\n'.format(len(atoms))

# Set the temperature to 300 K, and set the hot and cold heat bath
# temperatures to +-50% of this
alpha = 0.5
temp = 300
temp_cold = (1-alpha) * temp
temp_hot = (1+alpha) * temp

x, y, z = atoms.get_cell().diagonal()
Nslabs = 50
# Divide the simulation box into 50 pieces in the z direction
regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
# Use this division to assign different atoms to different slabs
slabs = AllSlabs(atoms, regions)
Nbaths = 2
# Define the first 2 slabs as cold heat baths
slabs.set_cold_slabs(range(0, Nbaths))
# Set the cold thermostat temperature to 0.5 * 300 K
slabs.set_cold_target_temperature(temp_cold)
# Define the last 2 slabs as hot heat baths
slabs.set_hot_slabs(range(Nslabs-Nbaths, Nslabs))
# Set the hot thermostat temperature to 1.5 * 300 K
slabs.set_hot_target_temperature(temp_hot)

atoms.center(vacuum=50.)

# Fix the ends
fix_ends_indices = get_zgnr_end_indices(width, length)
fix_ends_constraint = FixAtoms(indices=fix_ends_indices)

# Simulate a surface
rm = 3.0
surface_pos = atoms.positions[0][1] - rm
surface_constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0),
                     surface_pos, eps=20e-3, rm=rm, cutoff=10.0)

atoms.set_constraint([surface_constraint, fix_ends_constraint])

# Add initial velocities to atoms
MaxwellBoltzmannDistribution(atoms, temp*units.kB)

# Use a timestep of 0.2 fs, characteristic timescale of the thermostat
# of 600 fs, and on later part of the simulation, do the heat power
# averaging every 3 timesteps
dyn = NEMD(atoms, dt=0.2*units.fs, ttime=600*units.fs, Tave=100,
           slabs=slabs)

# Equilibrate for 1e4 steps
steps = int(1e4)
# Let the later part of the simulation, where the heat power averaging
# is done every 100 steps, be 1e4 steps long
steps2 = int(5e4)
stepstot = steps + steps2

# Save the temperature profile at a few timesteps to plot the
# temperature distribution evolution of the system
f_temp_profile = open('data/{0}_temp_profile.dat'.format(fname), 'w')
save_temp_profile(f_temp_profile, dyn)
dyn.attach(save_temp_profile, stepstot/40, f_temp_profile, dyn)

# Save the temperature distribution at a few timesteps to analyze
# the distributions and respective linear fits more closely
f_temp_distr = open('data/{0}_temp_distribution.dat'.format(fname), 'w')
save_temp_distribution(f_temp_distr, dyn)
dyn.attach(save_temp_distribution, stepstot/20, f_temp_distr, dyn)

# Save the temperature gradient at a few timesteps to analyze its
# evolution and convergence
f_temp_gradient = open('data/{0}_temp_gradient.dat'.format(fname), 'w')
save_temp_gradient(f_temp_gradient, dyn)
dyn.attach(save_temp_gradient, stepstot/40, f_temp_gradient, dyn)

# Save the energies at a few timesteps to analyze their fluctuations
# and conservation
f_energies = open('data/{0}_energies.dat'.format(fname), 'w')
dyn.attach(save_energies, stepstot/200, f_energies, dyn, atoms)

# Save the heat powers of both thermostats to analyze their
# fluctuations
f_heat_power = open('data/{0}_heat_power.dat'.format(fname), 'w')
dyn.attach(save_heat_power, steps/100, f_heat_power, dyn)

# Print steps to follow the progression of the simulation
dyn.attach(print_step, int(1e4), dyn)

# Save a trajectory
traj = PickleTrajectory('traj/{0}.traj'.format(fname), 'w', atoms)
dyn.attach(traj.write, stepstot/200)

# Use the LAMMPS calculator via the LAMMPSLib library, use the
# AIREBO potential
parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

# First run for given time to reach the steady-state
dyn.run(steps)

# Start calculating the heat transfer coefficient (multiplied by the
# cross section area and divided by length)
coeff, coeff_err = dyn.run_and_calc(steps2)

area = width_Ang * 1.42
coeff = coeff / area * length_Ang
coeff_err = coeff_err / area * length_Ang

# Change to SI units
joule = units.kJ / 1000
meter = 1e10
factor = 1. / (joule / (units.second*meter) )
coeff_si = factor * coeff 
coeff_err_si = factor * coeff_err

f_temp_profile.close()
f_temp_distr.close()
f_heat_power.close()
f_energies.close()

print '\nHeat transfer coefficient in ASE units:'
print '{0} +- {1}\n'.format(coeff, coeff_err)
print 'In SI units:'
print '{0} +- {1}\n'.format(coeff_si, coeff_err_si)

