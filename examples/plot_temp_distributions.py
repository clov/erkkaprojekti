from md_utilities import plot_temp_distributions
import sys

datafilename = sys.argv[1]

plot_temp_distributions(datafilename, 'figs/temp_distributions')
