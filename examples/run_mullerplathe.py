# Tested with Python 2.7.5, ASE 3.8.1, lammps-16Aug12 and the very 
# earliest version of the LAMMPSLib library; the newer LAMMPSlib
# library needs a newer version of LAMMPS.

from mullerplathe import MullerPlathe
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices 
from ase import units
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.constraints import FixAtoms
import sys, numpy as np
from helpfunctions import get_zgnr_width_angstroms
from md_utilities import save_temp_profile, save_temp_distribution,\
     save_energies, save_temp_gradient, ExternalForceLJ, print_step

fname = sys.argv[0].split('.')[0]

width = 5
length = 100
atoms = GNR(width, length, 'zigzag')
width_Ang = get_zgnr_width_angstroms(width) 
atoms.set_pbc(False)
atoms.center(vacuum=1.)

print 'Atoms in the simulation: {0}\n'.format(len(atoms))

x, y, z = atoms.get_cell().diagonal()
Nslabs = 50
# Divide the simulation box into 50 pieces in the z direction
regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
# Use this division to assign different atoms to different slabs
slabs = AllSlabs(atoms, regions)
Nbaths = 2
# Define the first 2 slabs as cold slabs (cold thermostats)
slabs.set_cold_slabs(range(0, Nbaths))
# Define the last 2 slabs as hot slabs (hot thermostats)
slabs.set_hot_slabs(range(Nslabs-Nbaths, Nslabs))

atoms.center(vacuum=50.)

# Fix the ends
fix_ends_indices = get_zgnr_end_indices(width, length)
fix_ends_constraint = FixAtoms(indices=fix_ends_indices)

# Simulate a surface
rm = 3.0
surface_pos = atoms.positions[0][1] - rm
surface_constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0),
                     surface_pos, eps=20e-3, rm=rm, cutoff=10.0)

atoms.set_constraint([surface_constraint, fix_ends_constraint])

# Add initial velocities to atoms
MaxwellBoltzmannDistribution(atoms, 300*units.kB)

# Do the velocity exchange every 80 timesteps and in the later part
# of the simulation, do the temperature gradient averaging every 100 
# timesteps
dyn = MullerPlathe(atoms, 0.2*units.fs, Texchange=80, Tave=100,
                   slabs=slabs, swap_symbol='C')

# Equilibrate for 1e4 steps
steps = int(1e4)
# Let the later part of the simulation, where the temperature gradient
# averaging is done every 100 steps, be 1e4 steps long
steps2 = int(5e4)
stepstot = steps + steps2

# Save the temperature profile at a few timesteps to plot the
# temperature distribution evolution of the system
f_temp_profile = open('data/{0}_temp_profile.dat'.format(fname), 'w')
save_temp_profile(f_temp_profile, dyn)
dyn.attach(save_temp_profile, stepstot/40, f_temp_profile, dyn)

# Save the temperature distribution at a few timesteps to analyze
# the distributions and respective linear fits more closely
f_temp_distr = open('data/{0}_temp_distribution.dat'.format(fname), 'w')
save_temp_distribution(f_temp_distr, dyn)
dyn.attach(save_temp_distribution, stepstot/20, f_temp_distr, dyn)

# Save the temperature gradient at a few timesteps to analyze its
# evolution and convergence
f_temp_gradient = open('data/{0}_temp_gradient.dat'.format(fname), 'w')
save_temp_gradient(f_temp_gradient, dyn)
dyn.attach(save_temp_gradient, stepstot/40, f_temp_gradient, dyn)

# Save the energies at a few timesteps to analyze their fluctuations
# and conservation
f_energies = open('data/{0}_energies.dat'.format(fname), 'w')
dyn.attach(save_energies, stepstot/200, f_energies, dyn, atoms)

# Print steps to follow the progression of the simulation
dyn.attach(print_step, int(1e4), dyn)

# Save a trajectory
traj = PickleTrajectory('traj/{0}.traj'.format(fname), 'w', atoms)
dyn.attach(traj.write, stepstot/200)

# Use the LAMMPS calculator via the LAMMPSlib library, use the
# AIREBO potential
# NOTE: The potential file has to be in the current directory!
parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
calc = LAMMPSLib(parameters=parameters)
atoms.set_calculator(calc)

# First run for given time to reach the steady-state
dyn.run(steps)

# Start calculating the heat transfer coefficient (multiplied by the
# cross section area)
coeff, coeff_err = dyn.run_and_calc(steps2)

area = width_Ang * 1.42
coeff /= area
coeff_err /= area

# Change to SI units
joule = units.kJ / 1000
meter = 1e10
factor = 1. / (joule / (units.second*meter) )
coeff_si = factor * coeff
coeff_err_si = factor * coeff_err 

f_temp_profile.close()
f_temp_distr.close()
f_temp_gradient.close()
f_energies.close()

print '\nHeat transfer coefficient in ASE units:'
print '{0} +- {1}\n'.format(coeff, coeff_err)
print 'In SI units:'
print '{0} +- {1}\n'.format(coeff_si, coeff_err_si)

