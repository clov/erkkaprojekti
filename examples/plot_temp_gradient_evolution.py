from md_utilities import plot_temp_gradient_evolution
import sys

datafilename = sys.argv[1]

plot_temp_gradient_evolution(datafilename, outputdir='figs')
