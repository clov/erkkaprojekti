import numpy as np
from helpfunctions import are_equal, transform_cartesian_to_cylindrical,\
                          identity, is_between
from ase import Atom, Atoms

class Region:
    """A class that represents a general region in 3D space."""
    
    def __init__(self, coordinates, limits, transform):
        self.coordinates = coordinates
        # TODO do some checking to limits, should be of the form
        # [(amin, amax), (bmin, bmax), (cmin, cmax)]
        self.limits = limits
        self.transform = transform
    
    def get_coordinates(self):
        return self.coordinates
    
    def get_coordinate_limits(self, coordinate):
        return self.limits[self.coordinates.index(coordinate)]
    
    def set_coordinate_limits(self, coordinate, coord_limits):
        coord_idx = self.coordinates.index(coordinate)
        self.limits[coord_idx] = coord_limits
        
    def get_mins(self):
        return self.limits[0][0], self.limits[1][0], self.limits[2][0]
    
    def get_maxs(self):
        return self.limits[0][1], self.limits[1][1], self.limits[2][1]      

    def get_centerpoint(self):
        """Returns the coordinates of the center point in the own coordinate
        system."""
        a, b, c = 0.5 * (np.array(self.get_maxs()) + np.array(self.get_mins()))
        return a, b, c

    def is_inside(self, obj):
        """Checks whether the given object is inside the region.
        
        The obj can be a 3D-point given in cartesian coordinates or an Atom
        object. Lower limits are inclusive and higher exclusive.
        """
        point = obj
        if isinstance(obj, Atom):
            point = obj.position
        
        transformed_point = self.transform(point)
        return is_between(transformed_point, self.get_mins(), self.get_maxs())    
        
    def get_atoms_inside(self, atoms):
        """Returns the atoms that are inside the region."""
        region_atoms = Atoms()
        for atom in atoms:
            if self.is_inside(atom):
                region_atoms += atom
        return region_atoms

    def get_kinetic_energy(self, atoms):
        """Returns the kinetic energy of the atoms inside the region."""
        region_atoms = self.get_atoms_inside(atoms)
        return region_atoms.get_kinetic_energy()

    def get_temperature(self, atoms):
        """Returns the temperature inside the region."""
        # TODO dimension?
        atoms_inside = self.get_atoms_inside(atoms)
        if len(atoms_inside) == 0:
            return 0.
        return atoms_inside.get_temperature()
    
    def __eq__(self, other):
        """Tests whether the limits are same within 1E-9."""
        return are_equal(self.limits, other.limits)

    def __ne__(self, other):
        return not self.__eq__(other)
        

class Regions:
    """A general container class for all kind of regions."""

    def __init__(self, regions):
        """Stores the given group of regions, which should be a list of Region
        objects."""
        self.regions = list(regions)

    def is_inside(self, obj):
        """Checks whether the given object is inside the group of regions.

        The obj can be a 3D-point given in cartesian coordinates or an Atom
        object. Lower limits are inclusive and higher exclusive.
        """
        for region in self.regions:
            if region.is_inside(obj):
                return True
        return False

    def get_atoms_inside(self, atoms):
        """Returns the atoms that are inside the group of regions."""
        group_atoms = Atoms()
        for region in self.regions:
            group_atoms += region.get_atoms_inside(atoms)
        return group_atoms

    def get_kinetic_energy(self, atoms):
        """Returns the kinetic energy of the atoms inside the group of
        regions."""
        return self.get_atoms_inside(atoms).get_kinetic_energy()

    def get_temperature(self, atoms):
        """Returns the temperature of the group of regions."""
        # TODO dimension?
        atoms_inside = self.get_atoms_inside(atoms)
        if len(atoms_inside) == 0:
            return 0. 
        return atoms_inside.get_temperature()
    
    def get_temperatures(self, atoms):
        """Returns the temperatures in each region in a list."""
        temps = []
        for region in self.regions:
            temps.append(region.get_temperature(atoms))
        return temps
    
    def __getitem__(self, key):
        return self.regions[key]
    
    def __len__(self):
        return len(self.regions)
    

class SliceRegions(Regions):
    """A container class for Region, which has an initializer to set up
    sliced Regions."""
    # TODO try to reduce the redundancy from BoxRegions and WedgeRegions


class BoxRegion(Region):
    """A class that represents a box-shaped region in 3D space."""

    coordinates = ('x', 'y', 'z')

    def __init__(self, (xmin, xmax), (ymin, ymax), (zmin, zmax)):
        """Use cartesian coordinates to define the region."""
        # TODO check limits
        limits = [(xmin, xmax), (ymin, ymax), (zmin, zmax)]
        transform = identity
        Region.__init__(self, BoxRegion.coordinates, limits, transform)

    def _check_limits(self, (xmin, xmax), (ymin, ymax), (zmin, zmax)):
        assert xmin < xmax
        assert ymin < ymax
        assert zmin < zmax


class BoxRegions(Regions, BoxRegion):
    """A container class for BoxRegion."""
    
    def __init__(self, (xmin, xmax), (ymin, ymax), (zmin, zmax), n, direction):
        """Makes a BoxRegion with the given arguments, slices the given
        direction into n parts and stores the resulting group of regions in a
        list."""
        self._check_limits((xmin, xmax), (ymin, ymax), (zmin, zmax))
        limits = [(xmin, xmax), (ymin, ymax), (zmin, zmax)]
        self.direction = direction
        self.regions = []
        
        dir_idx = BoxRegion.coordinates.index(self.direction)
        
        dir_limits = limits[dir_idx]
        length = (dir_limits[1] - dir_limits[0]) / n
        a = dir_limits[0]
        for i in range(n):
            region = BoxRegion((xmin, xmax), (ymin, ymax), (zmin, zmax))
            region.set_coordinate_limits(self.direction, (a, a+length))
            self.regions.append(region)
            a += length
        
        
class WedgeRegion(Region):
    """A class that represents a wedge-shaped region in 3D space."""

    coordinates = ('r', 'theta', 'z')

    def __init__(self, (rmin, rmax), (thetamin, thetamax), (zmin, zmax)):
        """Use cylindrical coordinates to define the region."""
        # TODO check limits
        limits = [(rmin, rmax), (thetamin, thetamax), (zmin, zmax)]
        transform = transform_cartesian_to_cylindrical
        Region.__init__(self, WedgeRegion.coordinates, limits, transform)

    def _check_limits(self, (rmin, rmax), (thetamin, thetamax), (zmin, zmax)):
        assert 0 <= rmin < rmax
        assert zmin < zmax
        assert -np.pi < thetamin <= np.pi
        assert -np.pi < thetamax <= np.pi
        assert thetamin < thetamax
        

class WedgeRegions(Regions, WedgeRegion):
    """A container class for WedgeRegion."""

    def __init__(self, (rmin, rmax), (thetamin, thetamax), (zmin, zmax),
                 n, direction='theta'):
        """Makes a WedgeRegion with the given arguments, slices the given
        direction (which should be 'theta') to n parts and stores the resulting
        group of regions in a list."""
        self._check_limits((rmin, rmax), (thetamin, thetamax), (zmin, zmax))
        limits = [(rmin, rmax), (thetamin, thetamax), (zmin, zmax)]
        self.direction = direction
        self.regions = []
        
        dir_idx = WedgeRegion.coordinates.index(self.direction)

        dir_limits = limits[dir_idx]
        length = (dir_limits[1] - dir_limits[0]) / n
        a = dir_limits[0]
        for i in range(n):
            region = WedgeRegion((rmin, rmax), (thetamin, thetamax),
                                 (zmin, zmax))
            region.set_coordinate_limits(self.direction, (a, a+length))
            self.regions.append(region)
            a += length


