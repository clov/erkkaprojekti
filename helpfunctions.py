import numpy as np
from ase import Atoms

def gcd(a, b):
    """Return greatest common divisor of a and b."""
    while b != 0:
        rem = a % b
        a, b = b, rem
    return a

def are_equal(a, b, precision=1E-9):
    """Tests whether two array-likes are equal in a given precision."""
    a = np.array(a)
    b = np.array(b)
    return np.all(np.abs(a-b) < precision)

def transform_cylindrical_to_cartesian((r, t, z)):
    x = r * np.cos(t)
    y = r * np.sin(t)
    return (x, y, z)

def transform_cartesian_to_cylindrical((x, y, z)):
    if x == 0 and y == 0:
        return (0., 0., z)
    r = np.sqrt(x*x + y*y)
    theta = np.arctan2(y, x)
    return (r, theta, z)

def identity(x):
    return x

def is_between(x, lower, upper):
    """Tests whether lower[i] <= x[i] < upper[i] is true for all i.
    All the arguments have to be 1D array-likes."""
    if np.shape(lower) != np.shape(upper) or np.shape(lower) != np.shape(x):
        raise TypeError('The array-likes must be of same shape.')
    for i in range(len(lower)):
        if x[i] < lower[i] or x[i] >= upper[i]:
            return False
    return True

def get_kinetic_energy(atom):
    """Returns the kinetic energy of an Atom object."""
    mom = atom.momentum
    return np.dot(mom, mom) / (2. * atom.mass)

def get_velocity(atom):
    """Returns the velocity of an Atom object."""
    return atom.momentum / atom.mass

def set_velocity(atom, vel):
    """Sets the velocity of an Atom object to vel."""
    atom.momentum = atom.mass * vel

def get_zgnr_width_angstroms(width_units, CC_length=1.42):
    # cos(60 deg) = 0.5
    return (width_units - 1) * CC_length\
           + width_units * CC_length * 0.5

def get_zgnr_length_angstroms(length_units, CC_length=1.42):
    #sin(60 deg) = sqrt(3)/2
    return (2*length_units - 1) * CC_length * np.sqrt(3)/2

def center_atoms(atoms, point):
    """Moves the *atoms*' center of mass to *point*."""
    com = np.array(atoms.get_center_of_mass())
    point = np.array(point)
    atoms.positions -= (com - point)

def setup_bending(atoms, angle, radius, rotation=0.0, physical=True):
    """
    Prepare a bending setup for a tube or slab.
    
    Tube should be originally periodic in z-direction with
    the correct cell, and optionally periodic in y-direction.
    
    Then atoms are set up for bending with hotbit.Wedge class
    with bending wrt. z-axis, and optionally periodic along
    z-axis.
    
    Atoms are first rotated pi/2 around -x-axis, then
    rotated an angle 'rotation' around y-axis, then
    transformed the distance 'radius' towards x-axis.
    The atoms are further adjusted for the wedge angle 'angle'.
    'physical' tells if the angle should be 2*pi/integer.
    """
    a = atoms
    a.rotate('-x', np.pi/2)
    L = a.get_cell().diagonal()
    r = a.get_positions()    
    if np.any( r[:, 1] < 0 ):
        raise AssertionError('For bending, all atoms should be above xy-plane')
    
    # move and adjust
    a.rotate('y', rotation)
    for i in range(len(a)):
        x, y = r[i, 0:2]
        R = x + radius
        phi = y * angle / L[2]
        r[i, 0] = R * np.cos(phi)
        r[i, 1] = R * np.sin(phi)
    a.set_positions(r)
    #return Atoms(a)
