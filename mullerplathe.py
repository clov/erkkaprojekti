from ase.md.verlet import VelocityVerlet
from helpfunctions import get_kinetic_energy, get_velocity, set_velocity
import numpy as np
from thermal_md import ThermalMD

class MullerPlathe(VelocityVerlet, ThermalMD):
    """Reverse non-equilibrium molecular dynamics, i.e. the Muller-Plathe
    method.

    This method is described in the paper of Muller-Plathe [1]. First,
    the simulation box is divided into slabs, each of which has a
    temperature defined by the equipartition theorem. A few of the
    first slabs are defined as "cold" slabs and a few of the last slabs
    are defined as "hot" slabs. Then after every *Texchange* timesteps,
    the velocities of the hottest atom in the cold slab and the coldest
    atom in the hot slab are exchanged. *swap_symbol* tells which atoms
    are considered, because the masses of the exchanged atoms have to
    be equal. These velocity-exchanges begin to increase the temperature
    in the hot slab and decrease it in the cold slab. In time, the system
    will reach a steady-state where the conductive heat transfer
    balances the heat transfer induced by the velocity exchanges.
    The run()-method is used to evolve the system for given number of
    timesteps to reach this steady-state.

    After reaching the steady-state, the run_and_calc()-method is used
    to start measuring and averaging the resulting temperature gradient,
    which is calculated from the slab temperatures by a linear
    least-squares fit.

    Finally, the thermal conductivity is defined by

        lambda = - J_z(t) / <dT(t,z)/dz>,

    where J_z(t) is the heat flux at the simulation time t in the
    direction of the slab-division (e.g. z-direction), dT(t,z)/dz
    is the temperature gradient at time t and <.> denotes
    time-averaging. The resulting thermal conductivity (multiplied
    by the cross section area) and its standard error are returned.

    [1] F. Muller-Plathe. A simple nonequilibrium molecular dynamics
        for calculating the thermal conductivity. The Journal of
        Chemical Physics, 106(14):6082--6085, 1997.

    Parameters:

        atoms : ase.Atoms
            An object that contains all the atoms in the system.
        dt : float
            Timestep.
        Texchange : int
            Period of the velocity-exchange processes, i.e.
            do the Muller-Plathe velocity-exchange every this many
            timesteps.
        Tave : int
            On later part of the simulation (when using run_and_calc
            method), do the temperature gradient averaging process
            every this many timesteps.
        slabs : slabs.AllSlabs
            An object that contains all the information about the
            slab-division.   
        swap_symbol : str
            Atomic symbol of the atoms that are to be exchanged in
            the velocity-exchanges.
        Nswap : int
            How many velocity-exchanges to do at a time.
    """

    def __init__(self, atoms, dt, Texchange, Tave, slabs, swap_symbol,
            Nswap=1, trajectory=None, logfile=None, loginterval=1):
        VelocityVerlet.__init__(self, atoms, dt, trajectory, logfile,
                                loginterval)
        self.Texchange = Texchange
        self.Tave = Tave
        self.slabs = slabs
        self.swap_symbol = swap_symbol
        self.Nswap = Nswap
        
        self._transferred_ekin = 0.

    def _get_special_atoms(self):
        """Returns the hottest atom in the cold heat bath and the coldest atom
        in the hot heat bath."""
        cold_slabs = self.slabs.get_cold_slabs()
        if cold_slabs == None:
            raise RuntimeError('No cold heat bath was set.')
        hot_slabs = self.slabs.get_hot_slabs()
        if hot_slabs == None:
            raise RuntimeError('No hot heat bath was set.')
        hottest_in_cold = cold_slabs.get_hottest_atom(self.swap_symbol,
                                                      'cold heat bath')
        coldest_in_hot = hot_slabs.get_coldest_atom(self.swap_symbol,
                                                    'hot heat bath')
        return hottest_in_cold, coldest_in_hot
        
    def _exchange_velocities(self, hot_atom, cold_atom):
        """Performs the Muller-Plathe velocity exchange.
        
        Performs the Muller-Plathe velocity exchange, where the velocities
        of the hottest atom in the cold heat bath (hot_atom) and the coldest
        atom in the hot heat bath (cold_atom) are exchanged. The exchange is
        done only if the hottest atom in the cold bath has more kinetic energy
        than the coldest atom in the hot bath.
        """
        transferred_ekin = get_kinetic_energy(hot_atom)\
                           - get_kinetic_energy(cold_atom)
        if transferred_ekin > 0:
            self._transferred_ekin += transferred_ekin
            
            hot_vel = get_velocity(hot_atom)
            cold_vel = get_velocity(cold_atom)
            set_velocity(hot_atom, cold_vel)
            set_velocity(cold_atom, hot_vel)
    
    def _calc_thermal_conductivity(self, t, temp_derivs_mean,
                                   temp_derivs_mean_err):
        """Returns the thermal conductivity times the cross section
        area and its standard error."""
        cond = self._transferred_ekin / (t * temp_derivs_mean)
        cond_err = self._transferred_ekin / (t * temp_derivs_mean**2)\
                       * temp_derivs_mean_err
        return cond, cond_err

    def step(self, forces):
        """First performs the Muller-Plathe velocity exchange and then the
        VelocityVerlet step."""
        if self.Texchange != 0:
            if np.mod(self.nsteps, self.Texchange) == 0:
                i = 0
                while i < self.Nswap:
                    hot_atom, cold_atom = self._get_special_atoms()
                    self._exchange_velocities(hot_atom, cold_atom)
                    i += 1
        return VelocityVerlet.step(self, forces)

    def run_and_calc(self, steps):
        """Start calculating the thermal conductivity.
        
        Start calculating the thermal conductivity, which includes
        calculating the transferred kinetic energy between the heat baths,
        elapsed time and time average of the temperature gradient.
        The gradient is averaged only every self.Tave timesteps.
        Use this only after a steady state has been established.

        Returns the thermal conductivity times the cross section area
        and its standard error.
        """
        self._transferred_ekin = 0.
        time_start = self.get_time()
        f = self.atoms.get_forces()
        
        if not self.atoms.has('momenta'):
            self.atoms.set_momenta(np.zeros_like(f))

        temp_derivs_sum = 0.
        temp_derivs_squared_err_sum = 0.
        steps_averaged = 0
        for step in xrange(steps):
            # Do the averaging every Tave timestep
            if np.mod(step, self.Tave) == 0:
                slope, _, slope_err = self.calc_temp_function()
                temp_derivs_sum += slope
                temp_derivs_squared_err_sum += slope_err * slope_err
                steps_averaged += 1

            f = self.step(f)
            self.nsteps += 1
            self.call_observers()
        
        time = self.get_time()
        elapsed_time = time - time_start
        temp_derivs_mean = temp_derivs_sum / steps_averaged
        # according to the standard propagation of error
        # delta mean(k) = 1/n*sqrt(sum_{i=0}^{n-1}(delta k_i)^2)
        temp_derivs_mean_err = np.sqrt(temp_derivs_squared_err_sum)\
                                       / steps_averaged
        return self._calc_thermal_conductivity(elapsed_time,
               temp_derivs_mean, temp_derivs_mean_err)
    
