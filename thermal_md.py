import numpy as np
from scipy import stats

class ThermalMD:
    """Base class for doing heat transfer calculations."""
    
    def get_centerpoints_and_temps(self):
        dir = self.slabs.direction
        centers_dir = self.slabs.get_centerpoints_in_direction(dir)
        temps = self.slabs.get_temperatures()
        return centers_dir, temps
    
    def calc_temp_function(self):
        """Makes a linear least-squares fit with the temperatures.
        
        Makes a linear least-squares fit where x = slab center points in the
        heat transfer direction and y = corresponding temperatures. This also
        ignores the slabs in the both ends which don't include any nonfixed
        atoms.
        """
        first = self.slabs.get_first_slabidx_with_nonfixed_atoms()
        last = self.slabs.get_last_slabidx_with_nonfixed_atoms()
        centers_dir, temps = self.get_centerpoints_and_temps()
        slope, intercept, _, _, slope_err\
            = stats.linregress(centers_dir[first:last], temps[first:last])
        return slope, intercept, slope_err
