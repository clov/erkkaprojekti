import os, pickle, sys
import numpy as np
import matplotlib.pyplot as plt

def print_step(dyn):
    """Prints the timestep of a running dynamic. Also calls
    sys.stdout.flush(), which is needed if one wants to follow the
    progression of a simulation whose output is redirected into a
    file."""
    print 'Step {}'.format(dyn.get_number_of_steps())
    sys.stdout.flush()

def save_energies(f, dyn, atoms):
    """Save the potential and kinetic energies at current timestep."""
    epot = atoms.get_potential_energy()
    ekin = atoms.get_kinetic_energy()
    data = dyn.get_number_of_steps(), epot, ekin
    pickle.dump(data, f)

def plot_energy_evolution(datafilename, outputdir=None,
        outputext='pdf', figsize=None, show=True):
    """Plot the potential, kinetic and total energy evolution during
    the simulation."""
    dir, filename = os.path.split(datafilename)
    if outputdir is None:
        outputdir = dir

    steps, epots, ekins, etots = [], [], [], []
    with open(datafilename, 'r') as f:
        while True:
            try:
                step, epot, ekin = pickle.load(f)
                etot = epot + ekin
                steps.append(step)
                epots.append(epot)
                ekins.append(ekin)
                etots.append(etot)
            except EOFError:
                break

    if figsize is not None:
        plt.figure(figsize=figsize)
    plt.plot(steps, np.array(ekins), 'b-', label='kinetic')
    plt.plot(steps, np.array(epots)-max(epots), 'k-', label='potential')
    plt.plot(steps, np.array(etots)-max(etots), 'g-', label='total')
    plt.xlabel('Step')
    plt.ylabel('Energy (eV)')
    plt.legend(loc='center right')

    filename_without_ext = os.path.splitext(filename)[0]
    outputpath = os.path.join(outputdir, filename_without_ext)
    outputpath_full = '{0}.{1}'.format(outputpath, outputext)
    plt.savefig(outputpath_full)

    if show:
        plt.show()
    plt.close()

def save_temp_profile(f, dyn):
    """Save the temperatures of the slabs at a timestep."""
    data = dyn.get_number_of_steps(), dyn.slabs.get_temperatures()
    pickle.dump(data, f)

def plot_temp_profile_evolution(datafilename, outputdir=None,
        outputext='pdf', figsize=None, show=True):
    """Plot the temperatures of the slabs at every timestep by a
    colorful temperature plot and show its evolution."""
    dir, filename = os.path.split(datafilename)
    if outputdir is None:
        outputdir = dir

    steps, temps = [], []
    with open(datafilename, 'r') as f:
        while True:
            try:
                step, temps_slabs = pickle.load(f)
                steps.append(step)
                temps.append(temps_slabs)
            except(EOFError):
                break

    temps0_len = len(temps[0]) 
    temps_len = len(temps)
    steps_len = len(steps)

    dx = steps[-1] / temps_len
    x = range(0, steps[-1]+1, dx)
    y = range(temps0_len+1)

    X, Y = np.meshgrid(x, y)
    colors = np.transpose(temps)

    if figsize is not None:
        plt.figure(figsize=figsize)

    plt.pcolormesh(X, Y, colors)
    cb = plt.colorbar()
    cb.set_label('Temperature (K)')
    plt.ylim([0, y[-1]])
    plt.xlabel('Step')
    plt.ylabel('Slab index')

    filename_without_ext = os.path.splitext(filename)[0]
    outputpath = os.path.join(outputdir, filename_without_ext)
    outputpath_full = '{0}.{1}'.format(outputpath, outputext)
    plt.savefig(outputpath_full)

    if show:
        plt.show()
    plt.close()

def save_temp_distribution(f, dyn):
    """Save the slab positions (in the heat transfer direction),
    temperatures of the slabs, timestep, slope and intercept of
    the linear fit to the temperatures, and the first and last
    slab indices that have atoms which are not fixed."""
    centers_dir, temps = dyn.get_centerpoints_and_temps()
    step = dyn.get_number_of_steps()
    slope, intercept, slope_err = dyn.calc_temp_function()
    first = dyn.slabs.get_first_slabidx_with_nonfixed_atoms()
    last = dyn.slabs.get_last_slabidx_with_nonfixed_atoms()
    data = centers_dir, temps, step, slope, intercept,\
           slope_err, first, last
    pickle.dump(data, f)

def plot_temp_distributions(datafilename, outputdir=None,
                        outputext='pdf', figsize=None):
    """Plot the temperature distributions (and linear fits) at each
    timestep."""
    dir, filename = os.path.split(datafilename)
    if outputdir is None:
        outputdir = dir
    with open(datafilename, 'r') as f:
        while True:
            try:
                data = pickle.load(f)
                centers_dir, temps, step, slope, intercept, _, first,\
                             last = data

                if figsize is not None:
                    plt.figure(figsize=figsize)
                plt.plot(centers_dir, temps, 'k.')
                x = np.linspace(centers_dir[first], centers_dir[last],
                                                    100)
                y = np.polyval([slope, intercept], x)
                plt.plot(x, y, 'r-')
                plt.xlim([centers_dir[0]-10, centers_dir[-1]+10])
                plt.xlabel('Slab position (Angstrom)')
                plt.ylabel('Temperature (K)')

                filename_without_ext = os.path.splitext(filename)[0]
                outputpath = os.path.join(outputdir,
                                          filename_without_ext)
                outputpath_full = '{0}_step_{1}.{2}'.format(outputpath,
                                    step, outputext)
                plt.savefig(outputpath_full)
                plt.close()
                
            except(EOFError):
                break

def save_temp_gradient(f, dyn):
    """Saves timestep, temperature gradient and its error into a file."""
    step = dyn.get_number_of_steps()
    slope, _, slope_err = dyn.calc_temp_function()
    data = step, slope, slope_err
    pickle.dump(data, f)

def plot_temp_gradient_evolution(datafilename, outputdir=None,
                     outputext='pdf', figsize=None, show=True):
    dir, filename = os.path.split(datafilename)
    if outputdir is None:
        outputdir = dir

    steps, slopes, slope_errs = [], [], []
    with open(datafilename, 'r') as f:
        while True:
            try:
                step, slope, slope_err = pickle.load(f)
                steps.append(step)
                slopes.append(slope)
                slope_errs.append(slope_err)
            except(EOFError):
                break

    steps = np.array(steps)
    slopes = np.array(slopes)
    slope_errs = np.array(slope_errs)
    #sort_indices = np.argsort(steps)
    if figsize is not None:
        plt.figure(figsize=figsize)
    #plt.plot(steps[sort_indices], slopes[sort_indices])
    plt.errorbar(steps, slopes, slope_errs, fmt='k.')
    extralim = 3./200 * steps[-1]
    plt.xlim([-extralim, steps[-1]+extralim])
    plt.xlabel('Step')
    plt.ylabel('Temperature gradient (K/Angstrom)')

    filename_without_ext = os.path.splitext(filename)[0]
    outputpath = os.path.join(outputdir, filename_without_ext)
    outputpath_full = '{0}.{1}'.format(outputpath, outputext)
    plt.savefig(outputpath_full)
    
    if show:
        plt.show()
    plt.close()

def save_heat_power(f, dyn):
    """Saves the amount of heat power to the bath in the left and the
    amount of heat power to the bath in the right at the given time.
    Can be used only with the NEMD dynamics.
    """
    data = (dyn.get_number_of_steps(), dyn.calc_heat_power_to_left(),
            dyn.calc_heat_power_to_right())
    pickle.dump(data, f)

def plot_heat_power_evolution(datafilename, outputdir=None,
                       outputext='pdf', figsize=None, show=True):
    """Plots different diagnostics about heat powers."""
    dir, filename = os.path.split(datafilename)
    if outputdir is None:
        outputdir = dir
        
    with open(datafilename, 'r') as f:
        steps, powers_left, powers_right = [], [], []
        while True:
            try:
                step, power_left, power_right = pickle.load(f)
                steps.append(step)
                powers_left.append(power_left)
                powers_right.append(power_right)
            except(EOFError):
                break

    powers_left = np.asarray(powers_left)
    powers_right = np.asarray(powers_right)
    powers = 0.5 * (powers_left - powers_right)
    powers_err = 0.5 * np.abs(powers_left + powers_right)

    if figsize is not None:
        plt.figure(figsize=figsize)
    leftlabel = '$P_z^\mathrm{L}$'
    rightlabel = '$P_z^\mathrm{R}$'
    label = '$(P_z^\mathrm{L}-P_z^\mathrm{R})/2$'
    #plt.plot(steps, powers_left, 'b-', label=leftlabel)
    #plt.plot(steps, powers_right, 'r-', label=rightlabel)
    plt.errorbar(steps, powers, powers_err, fmt='k.', label=label)
    plt.plot(steps, [0.]*len(steps), 'g--')

    plt.xlabel('Step')
    plt.ylabel('Heat power')
    plt.legend()

    filename_without_ext = os.path.splitext(filename)[0]
    outputpath = os.path.join(outputdir, filename_without_ext)
    outputpath_full = '{0}.{1}'.format(outputpath, outputext)
    plt.savefig(outputpath_full)
    
    if show:
        plt.show()
    plt.close()

def lennard_jones_force(r, eps, rm):
    """Returns the Lennard-Jones force between two objects with distance
    *r*.
    
    Uses the form V(r) = eps*((rm/r)**12 - 2*(rm/r)**6).
    """
    return eps * (12. * rm**12 / r**13 - 12. * rm**6 / r**7)

def trunc_lennard_jones_force(r, eps, rm, cutoff):
    """Returns the truncated Lennard-Jones force between two objects with
    distance *r* and given cutoff."""
    if r > cutoff:
        return 0.0
    f_lj = lennard_jones_force(r, eps, rm)
    f_lj_cutoff = lennard_jones_force(cutoff, eps, rm)
    return f_lj - f_lj_cutoff


class ExternalForceLJ:
    """A constraint that applies a Lennard-Jones type external force to atoms
    to simulate a surface below it."""
    
    def __init__(self, indices, direction, surface_pos, eps, rm, cutoff):
        """Apply the external force in given direction to atoms in the index
        list.
        
        The *surface_pos* tells the location of the surface by telling the
        distance between origo and the surface in *direction*, which has to
        be a 3-tuple, e.g. (0, 1, 0).
        """
        self.indices = indices
        self.dir = np.array(direction) / np.sqrt(np.dot(direction, direction))
        self.surface_pos = surface_pos
        self.eps = eps
        self.rm = rm
        self.cutoff = cutoff
    
    def adjust_positions(self, oldpositions, newpositions):
        pass
    
    def adjust_forces(self, positions, forces):
        for i in self.indices:
            pos_dir = np.dot(positions[i], self.dir)
            r = pos_dir - self.surface_pos
            lj_force = trunc_lennard_jones_force(r, self.eps, self.rm,
                                                 self.cutoff)
            forces[i] += self.dir * lj_force
    
    def copy(self):
        return ExternalForceLJ(self.indices, self.dir, self.surface_pos,
                               self.eps, self.rm, self.cutoff)
