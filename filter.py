from ase.constraints import Filter
from ase import Atoms

class EnhancedFilter(Filter):
    """Filter object with a few more methods."""
    
    def get_kinetic_energy(self):
        """Return the sum of the kinetic energies of the visible atoms."""
        atoms = Atoms()
        for i in self.index:
            atoms.append(self.atoms[i])
        return atoms.get_kinetic_energy()
    
    def get_temperature(self):
        """Return the temperature of the visible atoms."""
        atoms = Atoms()
        for i in self.index:
            atoms.append(self.atoms[i])
        return atoms.get_temperature()
    
    def set_cell(self, cell, scale_atoms=False):
        self.atoms.set_cell(cell, scale_atoms)