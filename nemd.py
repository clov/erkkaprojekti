from ase.md.md import MolecularDynamics
from ase.md.verlet import VelocityVerlet
from ase.md.npt import NPT
from ase.units import kB
from thermal_md import ThermalMD
import numpy as np

class NEMD(MolecularDynamics, ThermalMD):
    """Non-equilibrium molecular dynamics.
    
    Non-equilibrium molecular dynamics, where two heat baths are added to
    the ends of the system by introducing Nose-Hoover thermostats. The
    evolution of the 'normal' parts of the system is governed by
    Velocity Verlet dynamics.

    The atoms in the systems are first divided into slabs using the
    class slabs.AllSlabs. Each slab is defined to be either a 'normal',
    'cold' or a 'hot' slab while assuming that the hot and cold slabs are
    at the ends of the system. Also the target temperature of the hot
    and cold slabs are defined in the *slabs* object. The evolution of
    the atoms in the hot and cold slabs is governed by Nose-Hoover
    dynamics, while VelocityVerlet is used for the normal atoms.

    After reaching the steady-state with the run()-method, the
    run_and_calc()-method is used to start calculating the heat transfer
    coefficient, i.e. to start averaging the heat power going from the
    hot bath to the cold bath. The heat power P_k to the thermostat k can
    be calculated from the Nose-Hoover equations by
        
        P_k(t) = -3 zeta N_k k_B T_k(t),

    where zeta is the so-called "friction coefficient" in the Nose-Hoover
    equations, N_k is the number of atoms in the thermostat k, k_B is the
    Boltzmann constant and T_k(t) is the temperature of the thermostat k
    at time t. Because even at steady-state the heat power ejected from
    the hot bath does not equal the heat power injected to the cold
    bath, the heat power is defined as an average

        P(t) := 1/2 (P_L(t) - P_R(t)),

    where P_L is the heat power injected to the bath on the left and P_R
    is the heat power injected to the bath on the right. Moreover, the
    error of this is defined to be

        delta P(t) = 1/2 |P_L(t) + P_R(t)|.

    These are then averaged over time, i.e. <P(t)> is calculated, and
    the heat transfer coefficient (multiplied by the length L_z divided
    by the cross section area L_x L_y)
    
        lambda L_z/(L_x L_y) = - <P_z(t)> / Delta T

    is returned with its error. Here Delta T = T_R - T_L is the
    the (target) temperature difference of the heat baths.

    Parameters:

        atoms : ase.Atoms
            An object that contains all the atoms in the system.
        dt : float
            Timestep.
        ttime : float
            Characteristic timescale of the thermostats.
        Tave : int
            On later part of the simulation (when using run_and_calc
            method), do the heat power averaging process every
            this many timesteps.
        slabs : slabs.AllSlabs
            An object that includes the division of the atoms into normal,
            hot and cold slabs.
    """
    
    def __init__(self, atoms, dt, ttime, Tave, slabs, trajectory=None,
                 logfile=None, loginterval=1):
        MolecularDynamics.__init__(self, atoms, dt, trajectory, logfile,
                                   loginterval)
        self.ttime = ttime
        self.Tave = Tave
        self.slabs = slabs
        
        self.normal_atoms = self.slabs.get_normal_atoms()
        cold_atoms = self.slabs.get_cold_atoms()
        hot_atoms = self.slabs.get_hot_atoms()
        
        # The temperatures are in K!
        cold_temp = self.slabs.get_cold_target_temperature()
        hot_temp = self.slabs.get_hot_target_temperature()        

        # The hot and cold baths can be in either order, the other on
        # the left and the other on the right
        if self.slabs.is_hot_further_than_cold():
            self.left_atoms = cold_atoms
            self.right_atoms = hot_atoms
            self.left_temp = cold_temp
            self.right_temp = hot_temp
        else:
            self.left_atoms = hot_atoms
            self.right_atoms = cold_atoms
            self.left_temp = hot_temp
            self.right_temp = cold_temp
        
        # Nose-Hoover <=> use externalstress=0.0 and pfactor=None
        self.left_thermostat = NPT(self.left_atoms, self.dt,
                self.left_temp*kB, externalstress=0.0, ttime=self.ttime,
                pfactor=None)
        self.right_thermostat = NPT(self.right_atoms, self.dt,
                self.right_temp*kB, externalstress=0.0, ttime=self.ttime,
                pfactor=None)
        self.normal_dynamics = VelocityVerlet(self.normal_atoms, self.dt)

    def calc_heat_power_to_left(self):
        """Calculate the heat power from the system to the bath in the
        left."""
        return -3 * self.left_thermostat.zeta * len(self.left_atoms) * \
                kB * self.left_atoms.get_temperature()

    def calc_heat_power_to_right(self):
        """Calculate the heat power from the system to the bath in the
        right."""
        return -3 * self.right_thermostat.zeta * len(self.right_atoms) * \
                kB * self.right_atoms.get_temperature()
    
    def _calc_thermal_conductivity(self, heat_powers_mean,
                                   heat_powers_mean_err):
        """Calculates and returns the thermal conductivity times the
        cross section area divided by length.
        
        Returns also the standard error of this.
        """
        left_temp = self.left_temp
        right_temp = self.right_temp
        delta_temp = right_temp - left_temp
        cond = - heat_powers_mean / delta_temp
        cond_err = np.abs(heat_powers_mean_err / delta_temp)
        return cond, cond_err

    def run(self, steps):
        if not self.left_thermostat.initialized:
            self.left_thermostat.initialize()
        if not self.right_thermostat.initialized:
            self.right_thermostat.initialize()
        if self.left_thermostat.have_the_atoms_been_changed() or\
           self.right_thermostat.have_the_atoms_been_changed():
            raise NotImplementedError('You have modified the atoms since the \
last timestep')
            
        f = self.atoms.get_forces()
        if not self.normal_atoms.has('momenta'):
            self.normal_atoms.set_momenta(np.zeros_like(f))
        for step in xrange(steps):
            f = self.step(f)
            self.nsteps += 1
            self.call_observers()
        
    def step(self, f):
        # propagate the thermostats for one step
        self.left_thermostat.step()
        self.right_thermostat.step()
        
        # propagate the normal atoms with Velocity Verlet
        p = self.atoms.get_momenta() 
        p += 0.5 * self.dt * f
        newpos = self.atoms.get_positions() +\
            self.dt * p / self.atoms.get_masses()[:, np.newaxis]
        # set only the positions and momenta of the normal atoms
        self.normal_atoms.set_positions(newpos[self.normal_atoms.index])
        self.normal_atoms.set_momenta(p[self.normal_atoms.index]) 
        f = self.atoms.get_forces()
        p = self.atoms.get_momenta() + 0.5 * self.dt * f
        self.normal_atoms.set_momenta(p[self.normal_atoms.index])
        return f 

    def run_and_calc(self, steps):
        """Start calculating the thermal conductivity.

        Start calculating the thermal conductivity, i.e. start
        averaging the heat power. The heat power is averaged only
        every self.Tave timesteps. Use this only after a steady
        state has been established.

        Returns the thermal conductivity times the cross section area
        divided by length (and the corresponding standard error).
        """
        f = self.atoms.get_forces()

        if not self.atoms.has('momenta'):
            self.atoms.set_momenta(np.zeros_like(f))

        heat_powers_sum = 0.
        heat_powers_squared_err_sum = 0.
        steps_averaged = 0
        for step in xrange(steps):
            # Do the averaging every Tave timestep
            if np.mod(step, self.Tave) == 0:
                heat_power_left = self.calc_heat_power_to_left()
                heat_power_right = self.calc_heat_power_to_right()
                heat_power = 0.5 * (heat_power_left - heat_power_right)
                heat_powers_sum += heat_power
                heat_power_err = 0.5 * np.abs(heat_power_left\
                                              +heat_power_right)
                heat_powers_squared_err_sum += heat_power_err**2
                steps_averaged += 1

            f = self.step(f)
            self.nsteps += 1
            self.call_observers()

        heat_powers_mean = heat_powers_sum / steps_averaged
        # according to the standard propagation of error
        # delta mean(k) = 1/n*sqrt(sum_{i=0}^{n-1}(delta k_i)^2)
        heat_powers_mean_err = np.sqrt(heat_powers_squared_err_sum)\
                                       / steps_averaged
        return self._calc_thermal_conductivity(heat_powers_mean,
                                               heat_powers_mean_err)
