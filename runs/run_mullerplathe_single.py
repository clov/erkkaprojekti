from mullerplathe import MullerPlathe
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices 
from ase import units
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.constraints import FixAtoms
import sys, numpy as np
from helpfunctions import get_zgnr_width_angstroms
from md_utilities import save_temp_profile, save_temp_distribution,\
     ExternalForceLJ, print_step, save_energies, save_temp_gradient
from multiprocessing import Process, Queue

def run_mullerplathe_single(surface, surface_str, length, Nave,
                            fname, queue):
    fname += '_length_' + str(length) + '_Nave_' + str(Nave)

    width = 5
    atoms = GNR(width, length, 'zigzag')
    width_Ang = get_zgnr_width_angstroms(width) 
    atoms.set_pbc(False)
    atoms.center(vacuum=1.)

    x, y, z = atoms.get_cell().diagonal()
    Nslabs = length / 2
    regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
    slabs = AllSlabs(atoms, regions)
    Nbaths = length / 40
    slabs.set_cold_slabs(range(0, Nbaths))
    slabs.set_hot_slabs(range(Nslabs-Nbaths, Nslabs))

    atoms.center(vacuum=50.)

    # Fix the ends
    fix_ends_indices = get_zgnr_end_indices(width, length)
    fix_ends_constraint = FixAtoms(indices=fix_ends_indices)

    if surface:
        # Simulate a surface
        rm = 3.0
        surface_pos = atoms.positions[0][1] - rm
        surface_constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0),
                             surface_pos, eps=20e-3, rm=rm, cutoff=10.0)

        atoms.set_constraint([surface_constraint, fix_ends_constraint])

    else:
        atoms.set_constraint(fix_ends_constraint)

    MaxwellBoltzmannDistribution(atoms, 300*units.kB)

    dyn = MullerPlathe(atoms, 0.2*units.fs, 80, Nave, slabs, 'C')

    steps = int(5e5)
    steps2 = int(1e6)
    stepstot = steps + steps2

    f_temp_profile = open('{0}/data/{1}_temp_profile.dat'.format(surface_str,
                                                               fname), 'w')
    save_temp_profile(f_temp_profile, dyn)
    dyn.attach(save_temp_profile, stepstot/40, f_temp_profile, dyn)

    f_temp_distr = open('{0}/data/{1}_temp_distribution.dat'.format(surface_str,
                                                             fname), 'w')
    save_temp_distribution(f_temp_distr, dyn)
    dyn.attach(save_temp_distribution, stepstot/20, f_temp_distr, dyn)

    f_temp_gradient = open('{0}/data/{1}_temp_gradient.dat'.format(surface_str,
                                                              fname), 'w')
    save_temp_gradient(f_temp_gradient, dyn)
    dyn.attach(save_temp_gradient, stepstot/40, f_temp_gradient, dyn)

    f_energies = open('{0}/data/{1}_energies.dat'.format(surface_str,
                                                             fname), 'w')
    dyn.attach(save_energies, stepstot/200, f_energies, dyn, atoms)

    dyn.attach(print_step, int(1e4), dyn)

    traj = PickleTrajectory('{0}/traj/{1}.traj'.format(surface_str,
                                                       fname), 'w', atoms)
    if stepstot < 500:
        interval = 1
    else:
        interval = stepstot / 500
    dyn.attach(traj.write, interval)

    parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
    calc = LAMMPSLib(parameters=parameters)
    atoms.set_calculator(calc)


    dyn.run(steps)

    coeff, coeff_err = dyn.run_and_calc(steps2)


    area = width_Ang * 1.42
    coeff /= area
    coeff_err /= area
    joule = units.kJ / 1000
    meter = 1e10
    factor = 1. / (joule / (units.second*meter) )
    coeff_si = factor * coeff
    coeff_err_si = factor * coeff_err 

    f_temp_profile.close()
    f_temp_distr.close()
    f_temp_gradient.close()
    f_energies.close()

    queue.put((length, Nave, coeff_si, coeff_err_si))
