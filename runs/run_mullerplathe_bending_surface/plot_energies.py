import os, sys
from md_utilities import plot_energies

datapath = 'data/'
figpath = 'figs/'

fname = sys.argv[1]

plot_energies(datapath, fname, 'dat', figpath, 'png')
