import os
from md_utilities import plot_temp_profiles

datapath = 'data/'
figpath = 'figs/'

plot_temp_profiles(datapath, 'dat', figpath, 'png')
