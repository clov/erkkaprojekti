import pickle, sys
import matplotlib.pyplot as plt

fnames = ['run_mullerplathe_length_surface_coeffs.dat',
          'run_mullerplathe_length_nosurface_coeffs.dat']

lengths_all, coeffs_all, coeff_errs_all = [], [], []
for fname in fnames:
    lengths, coeffs, coeff_errs = [], [], []
    with open('data/{0}'.format(fname), 'r') as f:
        while True:
            try:
                length, coeff, coeff_err = pickle.load(f)
                lengths.append(length)
                coeffs.append(coeff)
                coeff_errs.append(coeff_err)
            except(EOFError):
                lengths_all.append(lengths)
                coeffs_all.append(coeffs)
                coeff_errs_all.append(coeff_errs)
                break

plt.figure(figsize=(5, 4.5))
fmts = ['k.', 'gx']
labels = ['With surface', 'Without surface']
for i in range(len(fnames)):
    plt.errorbar(lengths_all[i], coeffs_all[i],
                 coeff_errs_all[i], fmt=fmts[i],
                 label=labels[i])
plt.xlabel('Length (unit cells)')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')
plt.xlim([lengths[0]-10, lengths[-1]+10])
plt.ylim([120, 320])
plt.legend(loc='upper left')
plt.savefig('figs/run_mullerplathe_length_coeffs.pdf'.format(fname))
plt.show()
