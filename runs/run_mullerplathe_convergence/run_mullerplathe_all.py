from run_mullerplathe_single import run_mullerplathe_single
import sys, time, pickle
from multiprocessing import Process, Queue

def run_mullerplathe_all(surface):
    start = time.time()

    surface = True
    length = 130
    Naves = range(50, 10000, 500)

    if surface:
        surface_str = 'surface'
    else:
        surface_str = 'nosurface'
        
    fname = sys.argv[0].split('.')[0]
    queue = Queue()
    processes = [Process(target=run_mullerplathe_single,
                 args=(surface, surface_str, length, Nave, fname, queue))\
                 for Nave in Naves]
    print 'Starting {0} processes...'.format(len(processes))
    for process in processes:
        process.start()
    with open('{0}/data/{1}_coeffs.dat'.format(surface_str,
                                                fname), 'w') as f:
        for process in processes:
            pickle.dump(queue.get(), f)
            process.join()
    print 'Elapsed time: {0} s'.format(time.time() - start)
