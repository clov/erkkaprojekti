import os
from md_utilities import plot_temp_functions

datapath = 'data/temps/'
figpath = 'figs/temps/'

plot_temp_functions(datapath, 'dat', figpath, 'pdf', (5, 4.5))
