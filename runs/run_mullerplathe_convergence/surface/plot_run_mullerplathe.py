import pickle, sys, os
import matplotlib.pyplot as plt

datafilename = sys.argv[1]
outputdir = 'figs'
outputext = 'pdf'

Taves, coeffs, coeff_errs = [], [], []
with open(datafilename, 'r') as f:
    while True:
        try:
            _, Tave, coeff, coeff_err = pickle.load(f)
            Taves.append(Tave)
            coeffs.append(coeff)
            coeff_errs.append(coeff_err)
        except(EOFError):
            break

plt.figure(figsize=(5, 4.5))
plt.errorbar(Taves, coeffs, coeff_errs, fmt='k.')
plt.xlabel('$T_\mathrm{ave}$')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')

dir, filename = os.path.split(datafilename)
filename_without_ext = os.path.splitext(filename)[0]
outputpath = os.path.join(outputdir, filename_without_ext)
outputpath_full = '{0}.{1}'.format(outputpath, outputext)
plt.savefig(outputpath_full)
plt.show()
plt.close()

plt.figure(figsize=(5, 4.5))
plt.plot(Taves, coeff_errs, 'k.')
plt.xlabel('$T_\mathrm{ave}$')
plt.ylabel('$\delta\lambda\,(\mathrm{W/(mK))}$')
filename_without_ext += '_err'
outputpath = os.path.join(outputdir, filename_without_ext)
outputpath_full = '{0}.{1}'.format(outputpath, outputext)
plt.savefig(outputpath_full)
plt.show()
plt.close()
