import os, sys
from md_utilities import plot_temp_profile

datapath = 'data/'
figpath = 'figs/'

fname = sys.argv[1]

plot_temp_profile(datapath, fname, 'dat', figpath, 'pdf',
                  70, (5, 4.5))
