import pickle, sys
import matplotlib.pyplot as plt

fname = 'run_mullerplathe_length'

lengths, coeffs, coeff_errs = [], [], []
with open('data/{0}_coeffs.dat'.format(fname), 'r') as f:
    while True:
        try:
            length, coeff, coeff_err = pickle.load(f)
            lengths.append(length)
            coeffs.append(coeff)
            coeff_errs.append(coeff_err)
        except(EOFError):
            break

plt.figure(figsize=(5, 4.5))
plt.errorbar(lengths, coeffs, coeff_errs, fmt='k.')
plt.xlabel('Length (unit cells)')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')
plt.xlim([lengths[0]-10, lengths[-1]+10])
plt.ylim([160, 320])
plt.savefig('figs/{0}_coeffs.pdf'.format(fname))
plt.show()
