import os, sys
from md_utilities import plot_temp_profile_evolution

datafilename = sys.argv[1]

plot_temp_profile_evolution(datafilename, 'figs', 'pdf',
                            (5, 4.5))
