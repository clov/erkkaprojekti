import os, sys
from md_utilities import plot_slope_evolution

datapath = 'data/temps/'
figpath = 'figs/'

fname_start = sys.argv[1]

plot_slope_evolution(datapath, fname_start, 'dat', figpath, 'pdf')
