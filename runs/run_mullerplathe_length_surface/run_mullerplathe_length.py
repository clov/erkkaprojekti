from mullerplathe import MullerPlathe
from regions import BoxRegions
from slabs import AllSlabs
from graphene_structures import GNR, get_zgnr_end_indices 
from ase import units
from lammpsLib import LAMMPSLib
from ase.io.trajectory import PickleTrajectory
from ase.md.velocitydistribution import MaxwellBoltzmannDistribution
from ase.constraints import FixAtoms
import pickle, sys, os, time, numpy as np
from helpfunctions import get_zgnr_width_angstroms, setup_bending
from md_utilities import save_temp_profile, save_temp_function,\
     ExternalForceLJ, print_step, save_energies
from multiprocessing import Process, Queue

def run_mullerplathe(length, fname, queue):
    fname += '_' + str(length)

    width = 5
    atoms = GNR(width, length, 'zigzag')
    width_Ang = get_zgnr_width_angstroms(width) 
    atoms.set_pbc(False)
    atoms.center(vacuum=1.)

    x, y, z = atoms.get_cell().diagonal()
    Nslabs = length / 2
    regions = BoxRegions((0, x), (0, y), (0, z), Nslabs, 'z')
    slabs = AllSlabs(atoms, regions)
    Nbaths = length / 40
    slabs.set_cold_slabs(range(0, Nbaths))
    slabs.set_hot_slabs(range(Nslabs-Nbaths, Nslabs))

    # Fix the ends
    atoms.center(vacuum=50.)
    fix_ends_indices = get_zgnr_end_indices(width, length)
    fix_ends_constraint = FixAtoms(indices=fix_ends_indices)

    # Simulate a surface
    rm = 3.0
    surface_pos = atoms.positions[0][1] - rm
    surface_constraint = ExternalForceLJ(range(len(atoms)), (0, 1, 0),
                         surface_pos, eps=20e-3, rm=rm, cutoff=10.0)

    atoms.set_constraint([surface_constraint, fix_ends_constraint])

    MaxwellBoltzmannDistribution(atoms, 300*units.kB)

    dyn = MullerPlathe(atoms, 0.2*units.fs, 80, slabs, 'C')

    steps = int(1e6)
    steps2 = int(1e6)
    stepstot = steps + steps2

    f_temp = open('data/{0}_temp_profile.dat'.format(fname), 'w')
    save_temp_profile(f_temp, dyn)
    dyn.attach(save_temp_profile, stepstot/40, f_temp, dyn)
    f_temp_function = 'data/temps/{0}_temp_function'.format(fname)
    save_temp_function(dyn, f_temp_function)
    dyn.attach(save_temp_function, steps/10, dyn, f_temp_function)
    f_energies = open('data/{0}_energies.dat'.format(fname), 'w')
    dyn.attach(save_energies, steps/200, f_energies, dyn, atoms)
    #dyn.attach(print_step, 1, dyn)

    traj = PickleTrajectory('traj/{0}.traj'.format(fname), 'w', atoms)
    if stepstot < 500:
        interval = 1
    else:
        interval = stepstot / 500
    dyn.attach(traj.write, interval)

    parameters = ['pair_style airebo 3.0', 'pair_coeff * * CH.airebo C']
    #parameters = ['pair_style tersoff', 'pair_coeff * * SiCGe.tersoff C']
    calc = LAMMPSLib(parameters=parameters)
    atoms.set_calculator(calc)

    dyn.run(steps)

    dyn.attach(save_temp_function, steps2/10, dyn, f_temp_function)
    area = width_Ang * 1.42
    coeff, coeff_err = dyn.run_and_calc(steps2)
    coeff /= area
    coeff_err /= area
    joule = units.kJ / 1000
    meter = 1e10
    factor = 1. / (joule / (units.second*meter) )
    coeff_si = factor * coeff
    coeff_err_si = factor * coeff_err 

    f_temp.close()
    f_energies.close()

    queue.put((length, coeff_si, coeff_err_si))

if __name__ == '__main__':
    start = time.time()
    fname = sys.argv[0].split('.')[0]
    lengths = range(40, 140, 10)
    queue = Queue()
    processes = [Process(target=run_mullerplathe,
                         args=(length, fname, queue))\
                 for length in lengths]
    print 'Starting {0} processes...'.format(len(processes))
    for process in processes:
        process.start()
    with open('data/{0}_coeffs.dat'.format(fname), 'w') as f:
        for process in processes:
            pickle.dump(queue.get(), f)
            process.join()
    print 'Elapsed time: {0} s'.format(time.time() - start)
