import os, sys
from md_utilities import plot_energy_evolution

datafilename= sys.argv[1]

plot_energy_evolution(datafilename, 'figs/', 'pdf', (5, 4.5))
