import pickle, sys
import matplotlib.pyplot as plt

fname = 'run_nemd_surface'

ttimes, coeffs, coeff_errs = [], [], []
with open('data/{0}_coeffs.dat'.format(fname), 'r') as f:
    while True:
        try:
            ttime, coeff, coeff_err = pickle.load(f)
            ttimes.append(ttime)
            coeffs.append(coeff)
            coeff_errs.append(coeff_err)
        except(EOFError):
            break

plt.figure(figsize=(5, 4.5))
plt.errorbar(ttimes, coeffs, coeff_errs, fmt='k.')
plt.xlabel('ttime (fs)')
plt.ylabel('$\lambda\,(\mathrm{W/(mK))}$')
plt.savefig('figs/{0}_coeffs.pdf'.format(fname))
plt.show()
