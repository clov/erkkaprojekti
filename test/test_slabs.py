from slabs import Slab, Slabs, AllSlabs
from regions import BoxRegions
from ase import Atoms
from filter import EnhancedFilter
from helpfunctions import are_equal
import numpy as np
import pytest

class TestSlabs:

    def test_get_special_atoms(self):
        pos = [(0, 0, 0), (0, 1, 0),
               (0, 0, 1), (0, 1, 1),
               (0, 0, 2), (0, 1, 2),
               (0, 0, 3), (0, 1, 3)]
        vel = [(2, -2, 2), (0, 0, 1),
               (1, 0, 1), (-1, 2, 1),
               (5, 2, 4), (0, 0, 0.1),
               (1, 0, 0), (-5, 4, -2)]
        atoms = Atoms('C8', positions=pos)
        atoms.set_velocities(vel)
        slab0 = Slab(EnhancedFilter(atoms, indices=[0, 1]))
        slab1 = Slab(EnhancedFilter(atoms, indices=[2, 3]))
        slab2 = Slab(EnhancedFilter(atoms, indices=[4, 5]))
        slab3 = Slab(EnhancedFilter(atoms, indices=[6, 7]))

        slabs = Slabs([slab0])
        atom = slabs.get_hottest_atom('C')
        assert are_equal(atom.get_position(), atoms[0].get_position())
        assert are_equal(atom.get_momentum(), atoms[0].get_momentum())
        atom = slabs.get_coldest_atom('C')
        assert are_equal(atom.get_position(), atoms[1].get_position())
        assert are_equal(atom.get_momentum(), atoms[1].get_momentum())
        with pytest.raises(RuntimeError):
            slabs.get_hottest_atom('H')
            slabs.get_coldest_atom('H')

        slabs = Slabs([slab0, slab2])
        atom = slabs.get_hottest_atom('C')
        assert are_equal(atom.get_position(), atoms[4].get_position())
        assert are_equal(atom.get_momentum(), atoms[4].get_momentum())
        atom = slabs.get_coldest_atom('C')
        assert are_equal(atom.get_position(), atoms[5].get_position())
        assert are_equal(atom.get_momentum(), atoms[5].get_momentum())
        with pytest.raises(RuntimeError):
            slabs.get_hottest_atom('O')
            slabs.get_coldest_atom('O')

        slabs = Slabs([slab0, slab2, slab3])
        atom = slabs.get_hottest_atom('C')
        assert are_equal(atom.get_position(), atoms[4].get_position())
        assert are_equal(atom.get_momentum(), atoms[4].get_momentum())
        atom = slabs.get_coldest_atom('C')
        assert are_equal(atom.get_position(), atoms[5].get_position())
        assert are_equal(atom.get_momentum(), atoms[5].get_momentum())
        

class TestAllSlabs:

    def test_init(self):
        pos = [(0, 0, 0), (0, 1, 0),
               (0, 0, 1), (0, 1, 1),
               (0, 0, 2), (0, 1, 2),
               (0, 0, 3), (0, 1, 3)]
        atoms = Atoms('C8', positions=pos)
        regions = BoxRegions((-1, 1), (-1, 2), (-0.5, 3.5), 4, 'z')
        slabs = AllSlabs(atoms, regions)
        
        pos = slabs[0].get_atoms().get_positions()
        shouldbe = atoms[:2].get_positions()
        assert are_equal(pos, shouldbe) 
        assert are_equal(slabs[0].get_centerpoint(), (0., 0.5, 0.))

        pos = slabs[1].get_atoms().get_positions()
        shouldbe = atoms[2:4].get_positions()
        assert are_equal(pos, shouldbe)
        assert are_equal(slabs[1].get_centerpoint(), (0., 0.5, 1.))

        pos = slabs[2].get_atoms().get_positions()
        shouldbe = atoms[4:6].get_positions()
        assert are_equal(pos, shouldbe)
        assert are_equal(slabs[2].get_centerpoint(), (0., 0.5, 2.))

        pos = slabs[3].get_atoms().get_positions()
        shouldbe = atoms[6:].get_positions()
        assert are_equal(pos, shouldbe)
        assert are_equal(slabs[3].get_centerpoint(), (0., 0.5, 3.))

        assert len(slabs) == 4

        for slab in slabs:
            assert slab.slabtype == 'normal'

        assert slabs.get_cold_slabs() == None
        assert slabs.get_hot_slabs() == None

        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([2, 3])

        assert slabs[0].slabtype == 'cold'
        assert slabs[1].slabtype == 'normal'
        assert slabs[2].slabtype == 'hot'
        assert slabs[3].slabtype == 'hot'

        cold_slabs = slabs.get_cold_slabs()
        assert are_equal(cold_slabs[0].get_centerpoint(), (0., 0.5, 0.))
        assert len(cold_slabs) == 1

        hot_slabs = slabs.get_hot_slabs()
        assert are_equal(hot_slabs[0].get_centerpoint(), (0., 0.5, 2.))
        assert are_equal(hot_slabs[1].get_centerpoint(), (0., 0.5, 3.))
        assert len(hot_slabs) == 2

        cold_atoms = slabs.get_cold_atoms()
        assert np.all(cold_atoms.index == [0, 1])

        hot_atoms = slabs.get_hot_atoms()
        assert np.all(hot_atoms.index == [4, 5, 6, 7])

        normal_atoms = slabs.get_normal_atoms()
        assert np.all(normal_atoms.index == [2, 3])

    def test_centerpoints(self):
        pos = [(0, 0, i) for i in range(4)]
        atoms = Atoms('CHOSi', positions=pos)
        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 4, 'z')
        slabs = AllSlabs(atoms, regions)
        cps = slabs.get_centerpoints()
        cps_shouldbe = [(0, 0, i) for i in range(4)]
        assert are_equal(cps, cps_shouldbe)

        atoms.positions += 10.
        # center points should always remain the same, no matter where the
        # atoms are
        assert are_equal(cps, cps_shouldbe)

    def test_get_first_and_last_slab_with_nonfixed_atoms(self):
        pos = [(0, 0, i) for i in range(4)]
        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 4, 'z')

        # all atoms are nonfixed
        vel = [(1, 1, 1), (0, 2, 0), (3, 0, 1), (2, 1, 0)]

        atoms = Atoms('CHOSi', positions=pos)
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        atoms2 = atoms[1:]
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 1
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        atoms2 = atoms[:3]
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 2

        atoms2 = atoms[1:3]
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 1
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 2

        atoms2 = atoms[3:]
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 3
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3
        
        atoms2 = atoms[:1]
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 0

        atoms2 = Atoms([atoms[0], atoms[3]])
        slabs = AllSlabs(atoms2, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        atoms2 = Atoms()
        slabs = AllSlabs(atoms2, regions)
        # none of the slabs have atoms
        with pytest.raises(RuntimeError):
            slabs.get_first_slabidx_with_nonfixed_atoms()
        with pytest.raises(RuntimeError):
            slabs.get_last_slabidx_with_nonfixed_atoms()


        vel = [(1, 1, 1), (0, 0, 0), (3, 0, 1), (2, 1, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        vel = [(0, 0, 0), (0, 2, 0), (3, 0, 1), (2, 1, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 1
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        vel = [(0, 0, 0), (0, 0, 0), (3, 0, 1), (2, 1, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 2
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        vel = [(0, 0, 0), (0, 0, 0), (0, 0, 0), (2, 1, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 3 
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 3

        vel = [(0, 0, 0), (0, 0, 0), (0, 0, 0), (0, 0, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        with pytest.raises(RuntimeError):
            slabs.get_first_slabidx_with_nonfixed_atoms()
        with pytest.raises(RuntimeError):
            slabs.get_last_slabidx_with_nonfixed_atoms()

        vel = [(1, 1, 1), (0, 2, 0), (3, 0, 1), (0, 0, 0)]
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        assert slabs.get_first_slabidx_with_nonfixed_atoms() == 0
        assert slabs.get_last_slabidx_with_nonfixed_atoms() == 2


    def test_get_temperatures(self):
        pos = [(0, 0, i) for i in range(4)]
        atoms = Atoms('CHOSi', positions=pos)
        vel = [(-1, 0, 1), (2, 0, 0), (-1, -1, 1), (0, 0, 1)]
        atoms.set_velocities(vel)

        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 1, 'z')
        slabs = AllSlabs(atoms, regions)
        assert are_equal(slabs[0].get_temperature(), 100704.80742743785)
        temps = slabs.get_temperatures()
        assert are_equal(temps, [100704.80742743785])

        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 2, 'z')
        slabs = AllSlabs(atoms, regions)
        temps = slabs.get_temperatures()
        temps_shouldbe = [54258.064262668951, 147151.55059220674]
        assert are_equal(temps, temps_shouldbe)

        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 4, 'z')
        slabs = AllSlabs(atoms, regions)
        temps = slabs.get_temperatures()
        temps_shouldbe = [92920.67941821941, 15595.44910711849,
                          185664.19760434516, 108638.90358006833]
        assert are_equal(temps, temps_shouldbe, precision=1E-4)

    def test_set_target_temperature(self):
        atoms = Atoms()
        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 4, 'z')
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([3])
        slabs.set_cold_target_temperature(100)
        slabs.set_hot_target_temperature(300)
        assert slabs.get_cold_target_temperature() == 100
        assert slabs.get_hot_target_temperature() == 300
        
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0, 1])
        slabs.set_hot_slabs([2, 3])
        slabs.set_cold_target_temperature(50)
        slabs.set_hot_target_temperature(150)
        assert slabs.get_cold_target_temperature() == 50
        assert slabs.get_hot_target_temperature() == 150

    def test_is_hot_further_than_cold(self):
        atoms = Atoms()
        regions = BoxRegions((-1, 1), (-1, 1), (-0.5, 3.5), 4, 'z')
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([3])
        assert slabs.is_hot_further_than_cold() == True
        
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([3])
        slabs.set_hot_slabs([0])
        assert slabs.is_hot_further_than_cold() == False


