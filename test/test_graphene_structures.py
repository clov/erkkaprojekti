from graphene_structures import get_zgnr_end_indices

def test_get_zgnr_end_indices():
    shouldbe = [0, 2, 4, 6, 8, 199, 197, 195, 193, 191]
    assert get_zgnr_end_indices(5, 20) == shouldbe
    shouldbe = [0, 2, 4, 6, 8, 209, 207, 205, 203, 201]
    assert get_zgnr_end_indices(5, 21) == shouldbe
    shouldbe = [0, 2, 4, 6, 8, 10, 239, 237, 235, 233, 231, 229]
    assert get_zgnr_end_indices(6, 20) == shouldbe
    shouldbe = [0, 2, 4, 6, 8, 10, 251, 249, 247, 245, 243, 241]
    assert get_zgnr_end_indices(6, 21) == shouldbe
