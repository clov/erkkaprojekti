from mullerplathe import MullerPlathe
from regions import BoxRegions
from slabs import AllSlabs
from ase import Atoms
from helpfunctions import are_equal
import numpy as np
import pytest

class TestMullerPlathe:

    def test_get_special_atoms(self):
        pos = [(0, 0, 0), (0, 1, 0),
               (0, 0, 1), (0, 1, 1),
               (0, 0, 2), (0, 1, 2),
               (0, 0, 3), (0, 1, 3)]
        vel = [(2, -2, 2), (0, 0, 1),
               (1, 0, 1), (-1, 2, 1),
               (5, 2, 4), (0, 0, 0.1),
               (1, 0, 0), (-5, 4, -2)]
        atoms = Atoms('C8', positions=pos)
        atoms.set_velocities(vel)
        regions = BoxRegions((-1, 1), (-1, 2), (-0.5, 3.5), 4, 'z')

        slabs = AllSlabs(atoms, regions)
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        # no hot or cold baths are set
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()
        slabs.set_cold_slabs([0])
        # no hot bath is set
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()
        slabs = AllSlabs(atoms, regions)
        slabs.set_hot_slabs([3])
        # no cold bath is set
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([3])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hottest_atom, coldest_atom = dyn._get_special_atoms()
        assert hottest_atom.index == 0
        assert coldest_atom.index == 6
        
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'H')
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0, 1])
        slabs.set_hot_slabs([2, 3])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hottest_atom, coldest_atom = dyn._get_special_atoms()
        assert hottest_atom.index == 0
        assert coldest_atom.index == 5

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0, 2])
        slabs.set_hot_slabs([1, 3])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hottest_atom, coldest_atom = dyn._get_special_atoms()
        assert hottest_atom.index == 4
        assert coldest_atom.index == 6

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([2])
        slabs.set_hot_slabs([1])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hottest_atom, coldest_atom = dyn._get_special_atoms()
        assert hottest_atom.index == 4
        assert coldest_atom.index == 2

        regions = BoxRegions((-1, 1), (-1, 2), (-0.5, 4.5), 5, 'z')

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([4])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([4])
        slabs.set_hot_slabs([2])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()


        vel = [(2, -2, 2), (0, 0, 1),
               (1, 0, 1), (-1, 2, 1),
               (5, 2, 4), (0, 0, 0.1),
               # these atoms are fixed
               (0, 0, 0), (0, 0, 0)]

        atoms = Atoms('C8', positions=pos)
        atoms.set_velocities(vel)
        
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([3])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        # "no atoms" in the hot bath because all of them are fixed
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([3])
        slabs.set_hot_slabs([0])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        # "no atoms" in the cold bath because all of them are fixed
        with pytest.raises(RuntimeError):
            dyn._get_special_atoms()

        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([2])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hottest_atom, coldest_atom = dyn._get_special_atoms()
        assert hottest_atom.index == 0
        assert coldest_atom.index == 5

    def test_mpstep(self):
        pos = [(0, 0, 0), (0, 1, 0),
               (0, 0, 1), (0, 1, 1),
               (0, 0, 2), (0, 1, 2),
               (0, 0, 3), (0, 1, 3)]
        vel = [(2, -2, 2), (0, 0, 1),
               (1, 0, 1), (-1, 2, 1),
               (5, 2, 4), (0, 0, 0.1),
               (1, 0, 0), (-5, 4, -2)]
        atoms = Atoms('C8', positions=pos)
        regions = BoxRegions((-1, 1), (-1, 2), (-0.5, 3.5), 4, 'z')

        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([0])
        slabs.set_hot_slabs([3])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hot_atom, cold_atom = dyn._get_special_atoms()
        assert dyn._transferred_ekin == 0.
        dyn._exchange_velocities(hot_atom, cold_atom)
        vel_after = [(1, 0, 0), (0, 0, 1),
                     (1, 0, 1), (-1, 2, 1),
                     (5, 2, 4), (0, 0, 0.1),
                     (2, -2, 2), (-5, 4, -2)]
        assert np.all(atoms.get_velocities() == vel_after)
        assert are_equal(dyn._transferred_ekin, 66.06049999999999)
        
        atoms.set_velocities(vel)
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([2, 3])
        slabs.set_hot_slabs([0, 1])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hot_atom, cold_atom = dyn._get_special_atoms()
        dyn._exchange_velocities(hot_atom, cold_atom)
        vel_after = [(2, -2, 2), (5, 2, 4),
                     (1, 0, 1), (-1, 2, 1),
                     (0, 0, 1), (0, 0, 0.1),
                     (1, 0, 0), (-5, 4, -2)]
        assert np.all(atoms.get_velocities() == vel_after)

        # Note: now the velocities are vel_after
        slabs = AllSlabs(atoms, regions)
        slabs.set_cold_slabs([2])
        slabs.set_hot_slabs([1])
        dyn = MullerPlathe(atoms, 1., 1, slabs, 'C')
        hot_atom, cold_atom = dyn._get_special_atoms()
        # now there should be no exchanging, because hottest atom in the cold
        # bath has less kinetic energy than the coldest atom in the hot bath
        dyn._exchange_velocities(hot_atom, cold_atom)
        assert np.all(atoms.get_velocities() == vel_after)
        assert are_equal(dyn._transferred_ekin, 0.0)
