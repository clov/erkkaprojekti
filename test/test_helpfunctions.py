import pytest
from helpfunctions import are_equal, transform_cartesian_to_cylindrical,\
                          is_between, get_zgnr_width_angstroms, center_atoms
import numpy as np
from ase import Atoms

def test_are_equal():
    assert are_equal(1.0, 1.0) == True
    assert are_equal(1.0, 1.1) == False
    assert are_equal(1.0, 1.0+1E-10) == True
    assert are_equal(1.0, 1.0+1E-8) == False
    assert are_equal(-1.1, 1.1) == False
    assert are_equal(-1.1, -1.1-1E-10) == True
    assert are_equal([1, 1], [1., 1.]) == True
            
def test_transform_cartesian_to_cylindrical():
    transform = transform_cartesian_to_cylindrical
    assert are_equal(transform((0, 0, 0)), (0, 0, 0)) == True
    assert are_equal(transform((1, 1, 1)), (np.sqrt(2), np.pi/4, 1)) == True
    assert are_equal(transform((2, 2, 1)), (np.sqrt(8), np.pi/4, 1)) == True
    assert are_equal(transform((2, 1, 2)), (np.sqrt(5), np.arctan(0.5), 2)) == True
    assert are_equal(transform((0., 2., 1.)), (2, np.pi/2, 1)) == True
    assert are_equal(transform((-1, 1, 0)), (np.sqrt(2), 3*np.pi/4, 0)) == True
    assert are_equal(transform((-1, 0, 1)), (1, np.pi, 1)) == True
    assert are_equal(transform((-1, -1, 1)),
                    (np.sqrt(2), -3*np.pi/4, 1)) == True
    assert are_equal(transform((0, -1, 1)), (1, -np.pi/2, 1)) == True
    assert are_equal(transform((1, -1, 1)), (np.sqrt(2), -np.pi/4, 1)) ==  True

def test_is_between():
    lower = [-1, -1, -1]
    upper = [1, 1, 1]
    assert is_between([0, 0, 0], lower, upper) == True
    assert is_between([-2, 0, 0], lower, upper) == False
    assert is_between([2, 0, 0], lower, upper) == False
    assert is_between([0, -2, 0], lower, upper) == False
    assert is_between([0, 2, 0], lower, upper) == False
    assert is_between([0, 0, -2], lower, upper) == False
    assert is_between([0, 0, 2], lower, upper) == False
    assert is_between([0, -1.1, 0], lower, upper) == False
    
    assert is_between([0, 0, 0], upper, lower) == False
    assert is_between([0, 0, 0], [-1, 1, -1], [1, 1, 1]) == False

    with pytest.raises(TypeError):
        is_between([0, 0], [-1, -1, -1], [1, 1, 1])
    with pytest.raises(TypeError):
        is_between([0, 0, 0], [-1, -1], [1, 1, 1])
    with pytest.raises(TypeError):
        is_between([0, 0, 0], [-1, -1, -1], [1, 1])

def test_get_zgnr_width_angstroms():
    f = get_zgnr_width_angstroms
    assert are_equal(f(2), 2.84)
    assert are_equal(f(3), 4.97)
    assert are_equal(f(4), 7.1)
    assert are_equal(f(5), 9.23)

def test_center_atoms():
    pos = [(1, 1, 0), (-1, 1, 0), (-1, -1, 0), (1, -1, 0)]
    atoms = Atoms('C4', positions=pos)
    assert are_equal(atoms.get_center_of_mass(), (0., 0., 0.))
    center_atoms(atoms, (2, 2, 0))
    assert are_equal(atoms.get_center_of_mass(), (2., 2., 0.))
    center_atoms(atoms, (5, -3, 2))
    assert are_equal(atoms.get_center_of_mass(), (5., -3., 2.))
