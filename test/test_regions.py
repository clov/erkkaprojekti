import numpy as np
from regions import BoxRegion, BoxRegions, WedgeRegion, WedgeRegions
from helpfunctions import *
from ase import Atoms

class TestBoxRegion:

    def test_is_inside(self):
        region = BoxRegion((0, 2), (2, 4), (4, 6))
        assert region.is_inside((1, 3, 5)) == True
        assert region.is_inside((0, 2, 4)) == True
        assert region.is_inside((2, 4, 6)) == False
        assert region.is_inside((-1E-10, 3, 5)) == False
        assert region.is_inside((2+1E-10, 3, 5)) == False
        assert region.is_inside((1, 2-1E-10, 5)) == False
        assert region.is_inside((1, 4+1E-10, 5)) == False
        assert region.is_inside((1, 3, 4-1E-10)) == False
        assert region.is_inside((1, 3, 6+1E-10)) == False


class TestWedgeRegion:
    
    def test_is_inside(self):
        region = WedgeRegion((1, 2), (np.pi/6, np.pi/3), (-1, 1))
        transform = transform_cylindrical_to_cartesian
        assert region.is_inside(transform((1.5, np.pi/4, 0))) == True
        assert region.is_inside(transform((0.9, np.pi/4, 0))) == False
        assert region.is_inside(transform((1.0, np.pi/4, 0))) == True
        assert region.is_inside(transform((2.1, np.pi/4, 0))) == False
        assert region.is_inside(transform((1.5, np.pi/6-0.1, 0))) == False
        assert region.is_inside(transform((1.5, np.pi/6, 0))) == True
        assert region.is_inside(transform((1.5, np.pi/3+0.1, 0))) == False
        assert region.is_inside(transform((1.5, np.pi/4, -1.2))) == False
        assert region.is_inside(transform((1.5, np.pi/4, -1.0))) == True
        assert region.is_inside(transform((1.5, np.pi/4, 1.0))) == False
        assert region.is_inside(transform((1.5, np.pi/4, 1.2))) == False

    def test_eq(self):
        region0 = WedgeRegion((1, 2), (np.pi/6, np.pi/3), (-1, 1))
        region1 = WedgeRegion((1, 2.1), (np.pi/6, np.pi/3), (-1, 1))
        assert region0 != region1
        region1 = WedgeRegion((1-1E-10, 2+1E-10), (np.pi/6+1E-10, np.pi/3),
                              (-1, 1+1E-10))
        assert region0 == region1


class TestBoxRegions:

    def test_init(self):
        regions = BoxRegions((-2, 2), (1, 3), (-3, -1), n=1, direction='x')
        assert regions[0] == BoxRegion((-2, 2), (1, 3), (-3, -1))
        assert regions[0] != BoxRegion((-2, 2+1E-8), (1, 3), (-3, -1))
        assert regions[0] != BoxRegion((-2, 2), (1-1E-8, 3), (-3, -1))
        assert regions[0] != BoxRegion((-2, 2), (1, 3), (-3, -1+1E-8))
        
        regions = BoxRegions((-2, 2), (1, 3), (-3, -1), n=4, direction='x')
        assert regions[0] == BoxRegion((-2, -1), (1, 3), (-3, -1))
        assert regions[1] == BoxRegion((-1, 0), (1, 3), (-3, -1))
        assert regions[2] == BoxRegion((0, 1), (1, 3), (-3, -1))
        assert regions[3] == BoxRegion((1, 2), (1, 3), (-3, -1))
        assert regions[2] != BoxRegion((0, 1+1E-8), (1, 3), (-3, -1))

        regions = BoxRegions((-2, 2), (1, 4), (-3, -1), n=3, direction='y')
        assert regions[0] == BoxRegion((-2, 2), (1, 2), (-3, -1))
        assert regions[1] == BoxRegion((-2, 2), (2, 3), (-3, -1))
        assert regions[2] == BoxRegion((-2, 2), (3, 4), (-3, -1))
        assert regions[1] != BoxRegion((-2, 2), (2+1E-8, 3), (-3, -1))

        regions = BoxRegions((-2, 2), (1, 4), (-3, 0), n=3, direction='z')
        assert regions[0] == BoxRegion((-2, 2), (1, 4), (-3, -2))
        assert regions[1] == BoxRegion((-2, 2), (1, 4), (-2, -1))
        assert regions[2] == BoxRegion((-2, 2), (1, 4), (-1, 0))
        assert regions[2] != BoxRegion((-2, 2), (1, 4), (-1, -1E-8))


class TestWedgeRegions:

    def test_init(self):
        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=1)
        region = WedgeRegion((1, 2), (-np.pi/2, np.pi/2), (-1, 1))
        assert regions[0] == region
        region = WedgeRegion((1, 2), (-np.pi/2, np.pi/2+1E-8), (-1, 1))
        assert regions[0] != region
        
        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=2)
        region0 = WedgeRegion((1, 2), (-np.pi/2, 0), (-1, 1))
        region1 = WedgeRegion((1, 2), (0, np.pi/2), (-1, 1))
        assert regions[0] == region0
        assert regions[1] == region1
        region0 = WedgeRegion((1, 2), (-np.pi/2, -1E-8), (-1, 1))
        assert regions[0] != region0

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=4)
        region0 = WedgeRegion((1, 2), (-np.pi/2, -np.pi/4), (-1, 1))
        region1 = WedgeRegion((1, 2), (-np.pi/4, 0), (-1, 1))
        region2 = WedgeRegion((1, 2), (0, np.pi/4), (-1, 1))
        region3 = WedgeRegion((1, 2), (np.pi/4, np.pi/2), (-1, 1))
        assert regions[0] == region0
        assert regions[1] == region1
        assert regions[2] == region2
        assert regions[3] == region3
        assert regions[1] != WedgeRegion((1, 2), (-np.pi/4, 1E-8), (-1, -1))

    def test_is_inside(self):
        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=8)
        transform = transform_cylindrical_to_cartesian
        assert regions.is_inside(transform((1.5, -np.pi/2-1E-8, 0))) == False
        assert regions.is_inside(transform((1.5, -np.pi/2+1E-8, 0))) == True
        assert regions.is_inside(transform((1.5, -np.pi/4, 0))) == True
        assert regions.is_inside(transform((1.5, np.pi/4, 0))) == True
        assert regions.is_inside(transform((1.5, np.pi/2-1E-8, 0))) == True
        assert regions.is_inside(transform((1.5, np.pi/2+1E-8, 0))) == False
        assert regions.is_inside(transform((1.0-1E-8, 0, 0))) == False
        assert regions.is_inside(transform((2.0+1E-8, 0, 0))) == False
        assert regions.is_inside(transform((1.5, 0, -1-1E-8))) == False
        assert regions.is_inside(transform((1.5, 0, 1+1E-8))) == False

    def test_get_kinetic_energy(self):
        transform = transform_cylindrical_to_cartesian
        pos = [transform((1.5, -np.pi/3, 0)), transform((1.5, -np.pi/6, 0)),
               transform((1.5, np.pi/6, 0)), transform((1.5, np.pi/3, 0))]
        vel = [(-1, 0, 1), (2, 0, 0), (-1, -1, 1), (0, 0, 1)]
        atoms = Atoms('CHOSi', positions=pos)
        atoms.set_velocities(vel)

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=1)
        assert are_equal(regions.get_kinetic_energy(atoms), 52.068729999999)

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=2)
        assert are_equal(regions.get_kinetic_energy(atoms),
                         52.068729999999995)
        assert are_equal(regions.regions[0].get_kinetic_energy(atoms),
                         14.026879999999998)
        assert are_equal(regions.regions[1].get_kinetic_energy(atoms),
                         38.041849999999997)

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=4)
        assert are_equal(regions.get_kinetic_energy(atoms),
                         52.068729999999995)
        assert are_equal(regions.regions[0].get_kinetic_energy(atoms),
                         12.010999999999999)
        assert are_equal(regions.regions[1].get_kinetic_energy(atoms),
                         2.0158800000000001)
        assert are_equal(regions.regions[2].get_kinetic_energy(atoms),
                         23.999099999999999)
        assert are_equal(regions.regions[3].get_kinetic_energy(atoms),
                         14.04275)

    def test_get_temperature(self):
        transform = transform_cylindrical_to_cartesian
        pos = [transform((1.5, -np.pi/3, 0)), transform((1.5, -np.pi/6, 0)),
               transform((1.5, np.pi/6, 0)), transform((1.5, np.pi/3, 0))]
        vel = [(-1, 0, 1), (2, 0, 0), (-1, -1, 1), (0, 0, 1)]
        atoms = Atoms('CHOSi', positions=pos)
        atoms.set_velocities(vel)

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=1)
        assert are_equal(regions.get_temperature(atoms),
                         100704.80742743785)

        regions = WedgeRegions((1, 2), (-np.pi/2, np.pi/2), (-1, 1), n=2)
        assert are_equal(regions.get_temperature(atoms),
                         100704.80742743785)
        assert are_equal(regions.regions[0].get_temperature(atoms),
                         54258.064262668951)
        assert are_equal(regions.regions[1].get_temperature(atoms),
                         147151.55059220674)

