from helpfunctions import get_kinetic_energy
import numpy as np
from filter import EnhancedFilter

class Slab:
    """A class that represents a bunch of atoms in a larger system.
    
    The slab has certain atoms, a center point and a type.

        Parameters:

            atoms : filter.EnhancedFilter
                Atoms to be added to this slab
            slabtype : str
                Defines the slab to be a hot heat bath (slabtype='hot'),
                a cold heat bath (slabtype='cold') or a normal slab
                (slabtype='normal').
            target_temperature : float
                Sets the target temperature of the slab (in K), which
                is needed in a NEMD simulation. When doing an RNEMD
                (Muller-Plathe) simulation, the target temperature does
                not have any effect.
            centerpoint : 3-tuple
                A 3-tuple defining the center coordinates of the slab.
    """

    def __init__(self, atoms=None, slabtype='normal', target_temperature=None,
                 centerpoint=None):
        self.atoms = atoms
        self.set_slabtype(slabtype)
        self.set_target_temperature(target_temperature)
        self.centerpoint = centerpoint
    
    def get_atoms(self):
        """Returns the atoms that belong to the slab."""
        return self.atoms
    
    def is_empty(self):
        """Tells whether the slab contains atoms or not."""
        return len(self.get_atoms()) == 0
    
    def set_slabtype(self, slabtype):
        if slabtype not in ['normal', 'cold', 'hot']:
            raise ValueError("slabtype has to be either 'normal', 'cold' \
or 'hot'.")
        self.slabtype = slabtype
    
    def get_target_temperature(self):
        return self.target_temperature
    
    def set_target_temperature(self, temp):
        """Sets the target temperature of a hot or a cold slab."""
        if temp == None:
            self.target_temp = None
        elif self.is_normal():
            raise TypeError('You can only set the temperature of a cold or \
a hot slab.')
        elif temp <= 0:
            raise ValueError('Temperature has to be positive.')
        else:
            self.target_temperature = temp

    def get_centerpoint(self):
        return self.centerpoint

    def set_centerpoint(self, point):
        self.centerpoint = point

    def get_kinetic_energy(self):
        return self.get_atoms().get_kinetic_energy()
    
    def get_temperature(self):
        slab_atoms = self.get_atoms()
        if len(slab_atoms) == 0:
            return None
        return slab_atoms.get_temperature()
    
    def is_cold(self):
        """Tells whether the slab is a cold heat bath."""
        return self.slabtype == 'cold'
    
    def is_hot(self):
        """Tells whether the slab is a hot heat bath."""
        return self.slabtype == 'hot'
    
    def is_normal(self):
        """Tells whether the slab is 'normal'."""
        return self.slabtype == 'normal'
    
    def get_hottest_atom(self, symbol):
        """Returns the hottest atom (largest kinetic energy) in the slab
        with the given symbol."""
        hottest_atom = None
        hottest_ekin = -1.
        for atom in self.get_atoms():
            if atom.symbol == symbol:
                ekin = get_kinetic_energy(atom)
                if ekin > 0.0 and ekin > hottest_ekin:
                    hottest_atom = atom
                    hottest_ekin = ekin
        return hottest_atom
    
    def get_coldest_atom(self, symbol):
        """Returns the coldest atom (smallest kinetic energy) in the slab
        with the given symbol."""
        coldest_atom = None
        coldest_ekin = np.inf
        for atom in self.get_atoms():
            if atom.symbol == symbol:
                ekin = get_kinetic_energy(atom)
                if ekin > 0.0 and ekin < coldest_ekin:
                    coldest_atom = atom
                    coldest_ekin = ekin
        return coldest_atom

    def has_atoms_with_zero_energy(self):
        """Tells whether the slab contains atoms with zero kinetic energy.
        
        This happens if, for example, some of the atoms are fixed.
        """
        for atom in self.get_atoms():
            if get_kinetic_energy(atom) == 0:
                return True
        return False


class Slabs:
    """A general container class for Slab."""
    
    def __init__(self, slabs=None):
        if slabs == None:
            slabs = []
        self.slabs = slabs
    
    def append(self, slab):
        self.slabs.append(slab)
    
    def get_hottest_atom(self, symbol, msg='group of slabs'):
        """Returns the hottest atom (largest kinetic energy) inside the group
        of slabs."""
        hottest_atom = None
        hottest_ekin = -1.
        for slab in self.slabs:
            atom = slab.get_hottest_atom(symbol)
            if atom == None:
                continue
            ekin = get_kinetic_energy(atom)
            if ekin > hottest_ekin:
                hottest_atom = atom
                hottest_ekin = ekin
        if hottest_atom == None:
            raise RuntimeError('The hottest atom in the {} was not found \
because there were no unfixed atoms in that group with the given symbol (or atoms at \
all)'.format(msg))
        return hottest_atom
    
    def get_coldest_atom(self, symbol, msg='group of slabs'):
        """Returns the coldest atom (smallest kinetic energy) inside the group
        of slabs."""
        coldest_atom = None
        coldest_ekin = np.inf
        for slab in self.slabs:
            atom = slab.get_coldest_atom(symbol)
            if atom == None:
                continue
            ekin = get_kinetic_energy(atom)
            if ekin < coldest_ekin:
                coldest_atom = atom
                coldest_ekin = ekin
        if coldest_atom == None:
            raise RuntimeError('The coldest atom in the {} was not found \
because there were no unfixed atoms in that group with the given symbol (or atoms at \
all)'.format(msg))
        return coldest_atom
    
    def __getitem__(self, i):
        return self.slabs[i]
    
    def __len__(self):
        return len(self.slabs)


class AllSlabs(Slabs):
    """A container class for Slab to contain all the slabs in a system."""
    
    def __init__(self, atoms, regions):
        """Slices the atoms into slabs according to the slicing in the
        regions."""
        self.coordinates = regions.coordinates
        self.direction = regions.direction
        self.atoms = atoms
        self.slabs = Slabs()
        for region in regions:
            atoms_inside_idxs = []
            for atom in self.atoms:
                if region.is_inside(atom):
                    atoms_inside_idxs.append(atom.index)
            slab = Slab(EnhancedFilter(self.atoms, indices=atoms_inside_idxs))
            slab.set_centerpoint(region.get_centerpoint())
            self.slabs.append(slab)

    def get_centerpoints(self):
        return [slab.get_centerpoint() for slab in self.slabs]

    def get_centerpoints_in_direction(self, dir):
        """Returns the center point components in the direction *dir*."""
        centers = self.get_centerpoints()
        dir_idx = self.coordinates.index(self.direction)
        return np.array(centers)[:, dir_idx]

    def get_first_slabidx_with_nonfixed_atoms(self):
        """Returns the index of the first nonempty slab whose all atoms are
        nonfixed, i.e. their velocity != 0."""
        for i in range(len(self)):
            slab = self[i]
            if not slab.is_empty() and not slab.has_atoms_with_zero_energy():
                return i
        raise RuntimeError('None of the slabs had nonfixed atoms.')
    
    def get_last_slabidx_with_nonfixed_atoms(self):
        """Returns the index of the last nonempty slab whose all atoms are
        nonfixed, i.e. their velocity != 0."""
        for i in range(len(self))[::-1]:
            slab = self[i]
            if not slab.is_empty() and not slab.has_atoms_with_zero_energy():
                return i
        raise RuntimeError('None of the slabs had nonfixed atoms.')

    def get_temperatures(self):
        """Returns the temperatures of the slabs in a list."""
        temps = []
        for slab in self.slabs:
            temp = slab.get_temperature()
            if temp == None:
                temp = 0.
            temps.append(temp)
        return temps

    def get_normal_slabs(self):
        """Returns a Slabs-object containing the 'normal' slabs."""
        slabs = [slab for slab in self.slabs if slab.is_normal()]
        if len(slabs) == 0:
            return None
        return Slabs(slabs)
        
    def get_cold_slabs(self):
        """Returns a Slabs-object containing the cold slabs."""
        slabs = [slab for slab in self.slabs if slab.is_cold()]
        if len(slabs) == 0:
            return None
        return Slabs(slabs)
        
    def get_hot_slabs(self):
        """Returns a Slabs-object containing the hot slabs."""
        slabs = [slab for slab in self.slabs if slab.is_hot()]
        if len(slabs) == 0:
            return None
        return Slabs(slabs)
    
    def set_cold_slabs(self, idxs):
        """Sets all the slabs in the index list to be cold baths."""
        for i in idxs:
            self.slabs[i].set_slabtype('cold')
            
    def set_hot_slabs(self, idxs):
        """Sets all the slabs in the index list to be cold baths."""
        for i in idxs:
            self.slabs[i].set_slabtype('hot')
    
    def get_normal_atoms(self):
        """Returns the atoms inside the 'normal' slabs."""
        indices = []
        for slab in self.get_normal_slabs():
            for i in slab.get_atoms().index:
                indices.append(i)
        return EnhancedFilter(self.atoms, indices=indices)
    
    def get_cold_atoms(self):
        """Returns the atoms inside the cold bath slabs."""
        indices = []
        for slab in self.get_cold_slabs():
            for i in slab.get_atoms().index:
                indices.append(i)
        return EnhancedFilter(self.atoms, indices=indices)
    
    def get_hot_atoms(self):
        """Returns the atoms inside the hot bath slabs."""
        indices = []
        for slab in self.get_hot_slabs():
            for i in slab.get_atoms().index:
                indices.append(i)
        return EnhancedFilter(self.atoms, indices=indices)

    def set_cold_target_temperature(self, temp):
        """Sets the target temperature of the cold slabs."""
        for slab in self.get_cold_slabs():
            slab.set_target_temperature(temp)
    
    def set_hot_target_temperature(self, temp):
        """Sets the target temperature of the hot slabs."""
        for slab in self.get_hot_slabs():
            slab.set_target_temperature(temp)
        
    def get_cold_target_temperature(self):
        """Returns the target temperature of the cold slabs, which must be
        same for every cold slab."""
        return self.get_cold_slabs()[0].get_target_temperature()
    
    def get_hot_target_temperature(self):
        """Returns the target temperature of the hot slabs, which must be
        same for every hot slab."""
        return self.get_hot_slabs()[0].get_target_temperature()

    def is_hot_further_than_cold(self):
        """Returns true if the hot slabs are further in the direction of
        the slicing than the cold slabs."""
        dir_idx = self.coordinates.index(self.direction)
        return self.get_hot_slabs()[0].get_centerpoint()[dir_idx] \
             > self.get_cold_slabs()[0].get_centerpoint()[dir_idx]

