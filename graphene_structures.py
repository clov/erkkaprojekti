from ase import Atoms
from ase import Atom
import numpy as np
from math import sqrt, atan, cos, sin
from helpfunctions import gcd
vec = np.array
dot = np.dot
pi = np.pi

def get_zgnr_end_indices(width_units, length_units):
    """Returns the indices of the atoms in the both ends of a given ZGNR"""
    Natoms = 2 * width_units * length_units
    firsts = [2*n for n in range(width_units)]
    lasts = [Natoms-1-2*n for n in range(width_units)]
    return firsts + lasts

def graphene(n1,n2,R,height=5.0):
    """
    Construct graphene lattice, multiply the primitive cell
    n1 x n2 times in corresponding directions.

         .-----.
        /     /
       /   X / a2
      /     /
     X----->
      a1
    """
    if not isinstance(R,float): R=R[0]
    a1=vec([R*np.cos(pi/6)*2,0.,0.])
    a2=0.5*a1 + vec([0.,1.5*R,0.])
    #assert n2%2==0

    r=[]
    for i1 in range(n1):
        for i2 in range(n2):
            corner = i1*a1+i2*a2
            r.append(corner)
            r.append(corner+a1+vec([0.0,R,0.0]))

    cell=[[n1*a1[0], 0, 0],[n2*a2[0],n2*a2[1],0],[0,0,10]]
    atoms=Atoms('C'*len(r),positions=r,cell=cell)
    atoms.center(vacuum=height/2,axis=2)
    atoms.set_pbc((True,True,False))
    return atoms


def AGNR(n,units=1,saturated=False,sheet=False,C_C=1.42,C_H=1.09):
    """Make an n-armchair graphene nanoribbon running in z-direction
    
    Parameters:
    
    n : int
        ribbon width (atomic rows)
    units : int
        ribbon length (unit cells)
    saturated : bool
        Whether to place H atoms on the edge or not
    sheet : bool
        Whether to make an infinite sheet or not (x-z plane). Works only if
        n is even.
    C_C : float
        C-C bond length
    C_H : float
        C-H bond length
    """
    a = C_C * np.sqrt(3)
    atoms = Atoms()
    for i in range(n):
        x0 = 0.0
        if np.mod(i,2) == 1:
            x0 = 1.5 * C_C
        atoms += Atom('C',(x0+0,i*a/2,0))
        atoms += Atom('C',(x0+C_C,i*a/2,0))
        
    a = atoms.copy()
    for i in range(units-1):
        b = a.copy()
        b.translate(((i+1)*3*C_C,0,0))
        atoms += b
    
    atoms.set_pbc((False,False,True))
    atoms.rotate('z',np.pi/2)
    atoms.rotate('x',np.pi/2)
    atoms.set_cell((1,1,units*3*C_C))
    atoms.translate( -atoms.get_center_of_mass() )
     
    if saturated:
        # Edge C atoms are named so that it's intuitive when viewing
        # the graphene in following coordinate system:
        #      ^ z
        #      |
        # x    |
        # <----o y
        
        # C atoms in one period: 2n
        # atoms on the left (+x) side where H atoms should be pointing
        # downwards (-z)
        edge_idx_left_lower = np.array([ i*2*n for i in range(units) ])
        # atoms on the left (+x) side where H atoms should be pointing
        # upwards (+z)
        edge_idx_left_upper = edge_idx_left_lower + 1
        # right side (-x), H atoms pointing downwards (-z)
        edge_idx_right_lower = np.array([ 2*(n-1) + i*2*n for i in range(units) ])
        # right side (-x), H atoms pointing upwards (+z)
        edge_idx_right_upper = edge_idx_right_lower + 1
        
        all_edge_indexes = [ edge_idx_left_lower, edge_idx_left_upper,
                             edge_idx_right_lower, edge_idx_right_upper ]
        
        all_H_atoms = Atoms()
        # displacement norms in x and z direction
        dx = C_H * sqrt(3) / 2
        dz = C_H / 2
        displacements = [ vec([+dx,0,-dz]), vec([+dx,0,+dz]),
                          vec([-dx,0,-dz]), vec([-dx,0,+dz]) ]
        
        for edge_indexes, displacement in zip(all_edge_indexes,displacements):
            H_atoms = Atoms('H'*units)
            for i, edge_idx in enumerate(edge_indexes):
                H_atoms[i].set_position(atoms[edge_idx].get_position()+displacement)
            
            all_H_atoms += H_atoms
            
        atoms += all_H_atoms
        
    elif sheet:
        if np.mod(n,2) != 0:
            raise NotImplementedError('Can make sheet only if width is an even number')
        
        atoms.set_pbc((True,False,True))
        k, l, m = atoms.get_cell()
        k = (n/2*np.sqrt(3)*C_C,0,0)
        atoms.set_cell((k,l,m))
        
    return atoms


def ZGNR(n,units=1,saturated=False,sheet=False,C_C=1.42,C_H=1.09):
    """Make an n-zigzag graphene nanoribbon.

    Parameters:
    
    n : int
        ribbon width (atomic rows)
    units : int
        ribbon length (unit cells)
    saturated : bool
        Whether to place H atoms on the edge or not
    C_C : float
        carbon-carbon bond length
    C_H : float
        carbon-hydrogen bond length
    """
    a0 = C_C*np.sqrt(3)
    atoms = Atoms()
    for i in range(n):
        x = 1
        if np.mod(i,2)==1:
            x=-1
        atoms += Atom('C',(0,3*C_C*i/2+3*C_C/4-x*C_C/4,0))
        atoms += Atom('C',(a0/2,3*C_C*i/2+3*C_C/4+x*C_C/4,0))

    a = atoms.copy()
    for i in range(units-1):
        b = a.copy()
        b.translate(((i+1)*a0,0,0))
        atoms += b
        
    atoms.set_pbc((False,False,True))
    atoms.rotate('z',np.pi/2)
    atoms.rotate('x',np.pi/2)
    atoms.set_cell((n*3*C_C/2*1.2/2,1,units*a0))
    atoms.translate( -atoms.get_center_of_mass() )
    atoms.center(axis=2)
        
    if saturated:
        edge_idx_left = [ i*2*n for i in range(units) ]
        if np.mod(n,2) == 0:
            first = 2 * n - 2
        else:
            first = 2 * n - 1
        edge_idx_right = [ first + i*2*n for i in range(units) ]
        
        all_edge_indexes = [ edge_idx_left, edge_idx_right ]
        
        all_H_atoms = Atoms()
        displacements = [ vec([+C_H,0,0]), vec([-C_H,0,0]) ]
        
        for edge_indexes, displacement in zip(all_edge_indexes,displacements):
            H_atoms = Atoms('H'*units)
            for i, edge_idx in enumerate(edge_indexes):
                H_atoms[i].set_position(atoms[edge_idx].get_position()+displacement)
                
            all_H_atoms += H_atoms
            
        atoms += all_H_atoms
        
    return atoms
            
            
def GNR(n,units=1,type='armchair',saturated=False,sheet=False,C_C=1.42,C_H=1.09):
    """Make a graphene nanoribbon with zigzag or armchair edges running in z-direction
    
    Parameters:
    
    n : int
        Ribbon width (atomic rows)
    units : int
        Ribbon length (unit cells)
    type : string
        either 'zigzag' or 'armchair'
    saturated : bool
        Whether to place H atoms on the edge or not
    C_C : float
        C-C bond length
    C_H : float
        C-H bond length
    """
    if type == 'armchair':
        return AGNR(n, units, saturated, sheet, C_C, C_H)
    elif type == 'zigzag':
        return ZGNR(n, units, saturated, sheet, C_C, C_H)
